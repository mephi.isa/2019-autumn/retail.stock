import model.Section;
import model.Stock;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class SectionTest {

    private Stock stock = mock(Stock.class);
    private Section section = new Section(1, 30, stock, 25, 0.55);

    @Test
    void get_humidity_and_temperature_test() {
        assertEquals(25, section.getTemperature());
        assertEquals(0.55, section.getHumidity());
    }

    @Test
    void set_max_amount_test() {
        section.setMaxAmount(20);
        assertEquals(20, section.getMaxAmount());
        section.setMaxAmount(-2);
        assertEquals(0, section.getMaxAmount());
    }
}
