import model.Supply;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

public class SupplyTest {

    private Supply supply = new Supply(1, "provider", new Date());

    @Test
    void set_number_good() {
        supply.setNumber(20);
        assertEquals(supply.getNumber(), 20);
    }

    @Test
    void set_number_bad() {
        supply.setNumber(-2);
        assertEquals(supply.getNumber(), 0);
    }

    @Test
    void test_approve() {
        supply.approve();
        assertTrue(supply.getStatus().isApproved());
        supply.approve();
        assertTrue(supply.getStatus().isApproved());
    }

    @Test
    void test_decline() {
        supply.decline("reason");
        assertTrue(supply.getStatus().isDeclined());
        assertEquals("reason", supply.getStatus().getDescription());
        supply.decline("qwe qwe qwe");
        assertTrue(supply.getStatus().isDeclined());
        assertEquals("reason", supply.getStatus().getDescription());
    }
}
