import model.*;
import org.apache.commons.lang3.Range;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ConsignmentTest {
    private Supply supply = mock(Supply.class);
    private Consignment consignmentNormalHumidity = new Consignment("qwe", new Date(),
            30, supply, Range.between(10, 30), Range.between(0.00, 0.55));
    private Consignment consignmentIncorrectHumidity = new Consignment("qwe", new Date(12345),
            10, supply, Range.between(10, 30), Range.between(0.00, 1.01));

    @BeforeEach
    void before() {
        Section section = mock(Section.class);
        consignmentNormalHumidity.placing(section, 6, 1);
        consignmentNormalHumidity.placing(section, 7, 1);
    }

    @Test
    void test_incorrect_humidity() {
        assertEquals(0.00, consignmentIncorrectHumidity.getHumidityRange().getMinimum().doubleValue());
        assertEquals(0.50, consignmentIncorrectHumidity.getHumidityRange().getMaximum().doubleValue());
    }

    @Test
    void test_temperature_range() {
        assertEquals(10, consignmentNormalHumidity.getTemperatureRange().getMinimum().intValue());
        assertEquals(30, consignmentNormalHumidity.getTemperatureRange().getMaximum().intValue());
    }

    @Test
    void test_write_off() {
        consignmentNormalHumidity.writeOffByReason("standard reason", 1);
        assertNotNull(consignmentNormalHumidity.getWrittenOff());

        WrittenOff writtenOff = consignmentNormalHumidity.getWrittenOff();

        consignmentNormalHumidity.writeOffByReason("standard reason", 1);
        assertEquals(writtenOff, consignmentNormalHumidity.getWrittenOff());
        assertFalse(consignmentNormalHumidity.getWrittenOff().isApproved());

        consignmentIncorrectHumidity.writeOffByExpDate(1);
        assertNotNull(consignmentIncorrectHumidity.getWrittenOff());
        assertTrue(consignmentIncorrectHumidity.getWrittenOff().isApproved());
    }

    @Test
    void test_placing() {
        Section section = mock(Section.class);
        consignmentNormalHumidity.placing(section, 5, 1);
        assertEquals(18, consignmentNormalHumidity.getPlacedAmount());
    }

    @Test
    void test_for_sale_and_back_from_sale() {
        //было размещено 13 штук
        Section section = mock(Section.class);

        consignmentNormalHumidity.forSale(8, 1, 2); //в торговый зал 8
        assertEquals(5, consignmentNormalHumidity.getPlacedAmount()); // на складе 5
        assertEquals(8, consignmentNormalHumidity.getOnSaleAmount()); // в зале 8
        assertEquals(0, consignmentNormalHumidity.getReturnedAmount()); // возвращено 0

        consignmentNormalHumidity.backFromSale(3, 1); // возвращаем с торгового зала 3
        assertEquals(5, consignmentNormalHumidity.getPlacedAmount()); // на складе 5
        assertEquals(5, consignmentNormalHumidity.getOnSaleAmount()); // в зале 5
        assertEquals(3, consignmentNormalHumidity.getReturnedAmount()); // возвращено 3

        consignmentNormalHumidity.forSale(2, 1, 2); // в торговый зал 2
        assertEquals(3 ,consignmentNormalHumidity.getPlacedAmount()); // на складе 3
        assertEquals(7, consignmentNormalHumidity.getOnSaleAmount()); // в зале 7
        assertEquals(3, consignmentNormalHumidity.getReturnedAmount()); // возвращено 3

        consignmentNormalHumidity.placing(section, 4, 1 );
        assertEquals(7, consignmentNormalHumidity.getPlacedAmount()); // на складе 7
        assertEquals(7, consignmentNormalHumidity.getOnSaleAmount()); // в зале 7
        assertEquals(0, consignmentNormalHumidity.getReturnedAmount()); // возвращено 0

        consignmentNormalHumidity.forSale(12, 1, 2);
        assertEquals(7, consignmentNormalHumidity.getPlacedAmount()); // на складе 7
        assertEquals(7, consignmentNormalHumidity.getOnSaleAmount()); // в зале 7
        assertEquals(0, consignmentNormalHumidity.getReturnedAmount()); // возвращено 0

        consignmentNormalHumidity.backFromSale(12, 1);
        assertEquals(7, consignmentNormalHumidity.getPlacedAmount()); // на складе 7
        assertEquals(7, consignmentNormalHumidity.getOnSaleAmount()); // в зале 7
        assertEquals(0, consignmentNormalHumidity.getReturnedAmount()); // возвращено 0

        consignmentNormalHumidity.placing(section,32, 1);
        assertEquals(7, consignmentNormalHumidity.getPlacedAmount()); // на складе 7
        assertEquals(7, consignmentNormalHumidity.getOnSaleAmount()); // в зале 7
        assertEquals(0, consignmentNormalHumidity.getReturnedAmount()); // возвращено 0

        consignmentNormalHumidity.backFromSale( 4, 1);
        assertEquals(7, consignmentNormalHumidity.getPlacedAmount()); // на складе 7
        assertEquals(3, consignmentNormalHumidity.getOnSaleAmount()); // в зале 3
        assertEquals(4, consignmentNormalHumidity.getReturnedAmount()); // возвращено 4

        consignmentNormalHumidity.placing(section,2, 1);
        assertEquals(9, consignmentNormalHumidity.getPlacedAmount()); // на складе 9
        assertEquals(3, consignmentNormalHumidity.getOnSaleAmount()); // в зале 3
        assertEquals(2, consignmentNormalHumidity.getReturnedAmount()); // возвращено 2
    }

    @Test
    void test_set_amount() {
        Consignment consignmentNormalAmount = new Consignment("qwe", new Date(),
                1, supply, Range.between(10, 30), Range.between(0.00, 0.55));
        Consignment consignmentIncorrectAmount = new Consignment("qwe", new Date(),
                -2, supply, Range.between(10, 30), Range.between(0.00, 1.00));
        Consignment consignmentZeroAmount = new Consignment("qwe", new Date(),
                0, supply, Range.between(10, 30), Range.between(0.00, 1.00));
        assertEquals(1, consignmentNormalAmount.getAmount());
        assertEquals(0, consignmentIncorrectAmount.getAmount());
        assertEquals(0, consignmentZeroAmount.getAmount());
    }
}
