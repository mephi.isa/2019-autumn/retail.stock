import model.Consignment;
import model.WorkRecord;
import model.Section;
import org.apache.commons.lang3.Range;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class WorkRecordTest {

    private Consignment consignment = mock(Consignment.class);
    private Section section = mock(Section.class);
    private WorkRecord consignmentInSection = new WorkRecord(consignment, section, 0, 1);

//    @Test
//    void set_amount_no_such_unplaced_amount() {
//        when(consignment.getUnplacedAmount()).thenReturn(5);
//        consignmentInSection.setAmount(6);
//        assertEquals(0, consignmentInSection.getAmount());
//    }

    @Test
    void set_amount_less_than_zero() {
        when(consignment.getUnplacedAmount()).thenReturn(5);
        consignmentInSection.setAmount(-1);
        assertEquals(0, consignmentInSection.getAmount());
    }

    @Test
    void set_amount_good() {
        when(consignment.getUnplacedAmount()).thenReturn(5);
        consignmentInSection.setAmount(5);
        assertEquals(5, consignmentInSection.getAmount());
    }

    @Test
    void storage_conditions_is_good() {
        Range<Integer> temperatureRange = Range.between(-20, 30);
        Range<Double> humidityRange = Range.between(0.01, 0.50);
        when(consignment.getTemperatureRange()).thenReturn(temperatureRange);
        when(consignment.getHumidityRange()).thenReturn(humidityRange);
        when(section.getTemperature()).thenReturn(10);
        when(section.getHumidity()).thenReturn(0.35);
        assertTrue(consignmentInSection.storageConditionsIsGood());
    }

    @Test
    void storage_conditions_is_good_temp_high() {
        Range<Integer> temperatureRange = Range.between(-20, 30);
        Range<Double> humidityRange = Range.between(0.01, 0.50);
        when(consignment.getTemperatureRange()).thenReturn(temperatureRange);
        when(consignment.getHumidityRange()).thenReturn(humidityRange);
        when(section.getTemperature()).thenReturn(40);
        when(section.getHumidity()).thenReturn(0.35);
        assertFalse(consignmentInSection.storageConditionsIsGood());
    }

    @Test
    void storage_conditions_is_good_humidity_high() {
        Range<Integer> temperatureRange = Range.between(-20, 30);
        Range<Double> humidityRange = Range.between(0.01, 0.50);
        when(consignment.getTemperatureRange()).thenReturn(temperatureRange);
        when(consignment.getHumidityRange()).thenReturn(humidityRange);
        when(section.getTemperature()).thenReturn(30);
        when(section.getHumidity()).thenReturn(0.51);
        assertFalse(consignmentInSection.storageConditionsIsGood());
    }
}
