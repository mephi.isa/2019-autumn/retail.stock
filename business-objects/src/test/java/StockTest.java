import model.Section;
import model.Stock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StockTest {

    private Stock commonStock;

    @BeforeEach
    void before() {
        commonStock = new Stock(1, "Gagarina 25");
        commonStock.getSections().add( new Section(1, 25, commonStock, 12, 0.45));
        commonStock.getSections().add(new Section(2, 2, commonStock, 123, 0.44));
        commonStock.getSections().add(new Section(3, 15, commonStock, 25, 0.23));
    }

    @Test
    void test_create_stock() {
        Stock stock = new Stock(1, "Moskovskaya 29");
        assertEquals(1, stock.getNumber());
        stock = new Stock(-2, "Moskovskaya 29");
        assertEquals(0, stock.getNumber());
    }

    @Test
    void test_add_section() {
        commonStock.getSections().add(new Section(1, 12, commonStock, 25, 0.23));
        assertEquals(4, commonStock.getSections().size());
        commonStock.getSections().add(new Section(1, 12, commonStock, 25, 0.23));
        assertEquals(5, commonStock.getSections().size());
    }

    @Test
    void test_remove_section() {
        commonStock.getSections().remove(commonStock.getSections().stream().findFirst().get());
        assertEquals(2, commonStock.getSections().size());
        commonStock.getSections().remove(commonStock.getSections().stream().findFirst().get());
        assertEquals(1, commonStock.getSections().size());
        commonStock.getSections().remove(commonStock.getSections().stream().findFirst().get());
        assertEquals(0, commonStock.getSections().size());
    }

    @Test
    void get_all_max_amount_of_products() {
        assertEquals(42, commonStock.getSections().stream().mapToInt(Section::getMaxAmount).sum());
    }

}
