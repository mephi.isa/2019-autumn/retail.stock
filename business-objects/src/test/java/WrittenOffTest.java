import model.Consignment;
import model.WrittenOff;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class WrittenOffTest {

    @Test
    void test_approve() {
        WrittenOff writtenOff = new WrittenOff(1, mock(Consignment.class), "standard reason");
        assertFalse(writtenOff.isApproved());
        writtenOff.approve();
        assertTrue(writtenOff.isApproved());
        Date previousDate = writtenOff.getDate();
        writtenOff.approve();
        assertEquals(previousDate, writtenOff.getDate());
    }
}
