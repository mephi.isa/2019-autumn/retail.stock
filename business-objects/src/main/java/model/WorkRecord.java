package model;

import java.util.Date;

public class WorkRecord {
    private Consignment consignment;
    private Section section;
    private int amount;
    private long userInvokerId;
    private long userRecieverId;
    //private long
    private Date date;

    public WorkRecord(Consignment consignment, Section section, int amount, long userInvokerId, long userRecieverId, Date date) {
        this.consignment = consignment;
        this.section = section;
        setAmount(amount);
        this.userInvokerId = userInvokerId;
        this.userRecieverId = userRecieverId;
        this.date = date;
    }

    public WorkRecord(Consignment consignment, Section section, int amount, long userInvokerId) {
        this.consignment = consignment;
        this.section = section;
        setAmount(amount);
        this.userInvokerId = userInvokerId;
        this.userRecieverId = 0;
        this.date = new Date();
    }

    public WorkRecord(Consignment consignment, int amount, long userInvokerId) {
        this.consignment = consignment;
        this.section = null;
        setAmount(amount);
        this.userInvokerId = userInvokerId;
        this.userRecieverId = 0;
        this.date = new Date();
    }

    public WorkRecord(Consignment consignment, int amount, long userInvokerId, long userRecieverId) {
        this.consignment = consignment;
        this.section = null;
        setAmount(amount);
        this.userInvokerId = userInvokerId;
        this.userRecieverId = userRecieverId;
        this.date = new Date();
    }

    public void setAmount(int amount) {
        //if (amount <= consignment.getUnplacedAmount() && amount > 0)
        if (amount>0)
            this.amount = amount;
        else
            this.amount = 0;
    }

    public boolean storageConditionsIsGood() {
        return consignment.getTemperatureRange().contains(section.getTemperature()) && consignment.getHumidityRange().contains(section.getHumidity());
    }

 /*   public Consignment getConsignment() {
        return consignment;
    }*/

    public int getAmount() {
        return amount;
    }

 /*   public long getUserId() {
        return userId;
    }*/

    public Section getSection() {
        return section;
    }

    public long getUserInvokerId() {
        return userInvokerId;
    }

    public void setUserInvokerId(long userInvokerId) {
        this.userInvokerId = userInvokerId;
    }

    public long getUserRecieverId() {
        return userRecieverId;
    }

    public void setUserRecieverId(long userRecieverId) {
        this.userRecieverId = userRecieverId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

 /*   @Override
    public String toString() {
        return String.valueOf(getAmount());
    }*/
}
