package model;

public class Status {
    private boolean declined;
    private boolean approved;
    private String description;
    private Supply supply;

    public Status(boolean isDeclined, boolean isApproved, String description){
        this.declined = isDeclined;
        this.approved = isApproved;
        this.description = description;
        this.supply = null;
    }

    public Status(Supply supply) {
        this.declined = false;
        this.approved = false;
        this.description = "";
        this.supply = supply;
    }

    public void setSupply (Supply supply){
        this.supply = supply;
    }

    public boolean isDeclined() {
        return declined;
    }

    public boolean isApproved() {
        return approved;
    }

    public String getDescription() {
        return description;
    }

   /* public Supply getSupply() {
        return supply;
    }*/

    public void setDeclined(boolean declined) {
        this.declined = declined;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public void setDescription(String description) {
        this.description = description;
    }

   /* public void setSupply(Supply supply) {
        this.supply = supply;
    }*/
}
