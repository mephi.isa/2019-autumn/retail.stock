package model;

import org.apache.commons.lang3.Range;

import java.util.*;
import java.util.stream.Collectors;

public class Consignment {
    private String article;
    private Date expDate;
    private int amount;
    private int returnedAmount;
    private Supply supply;
    private Map<Operations, List<WorkRecord>> operations;
    private WrittenOff writtenOff;
    private Range<Integer> temperatureRange;
    private Range<Double> humidityRange;

    public Consignment(String article, Date expDate, int amount, Supply supply,
                       Range<Integer> temperatureRange, Range<Double> humidityRange) {
        this.article = article;
        this.expDate = expDate;
        setAmount(amount);
        this.supply = supply;
        this.temperatureRange = temperatureRange;
        setHumidityRange(humidityRange);
        this.operations = new HashMap<>();
        this.operations.put(Operations.ForSale, new LinkedList<>());
        this.operations.put(Operations.Incoming, new LinkedList<>());
        this.operations.put(Operations.Returned, new LinkedList<>());
        this.returnedAmount = 0;
    }

    public Consignment(String article, Date expDate, int amount, Supply supply,
                       Range<Integer> temperatureRange, Range<Double> humidityRange,
                       Map<Operations, List<WorkRecord>> operations, int returnedAmount ) {
        this.article = article;
        this.expDate = expDate;
        setAmount(amount);
        this.supply = supply;
        this.temperatureRange = temperatureRange;
        setHumidityRange(humidityRange);
        this.operations = operations;
        this.returnedAmount = returnedAmount;
    }

    public Map<Operations, List<WorkRecord>> getOperations(){
        return operations;
    }

    public String getArticle() {
        return article;
    }

    public boolean isExpired(Date tgtDate){
        return tgtDate.after(expDate);
    }

    public Date getExpDate() {
        return expDate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        if (amount > 0)
            this.amount = amount;
        else
            this.amount = 0;
    }

    public Supply getSupply() {
        return supply;
    }

//    public List<Returned> getReturnedList() {
//        return returnedList;
//    }

    public Range<Double> getHumidityRange() {
        return humidityRange;
    }

    public Range<Integer> getTemperatureRange() {
        return temperatureRange;
    }

    public WrittenOff getWrittenOff() {
        return writtenOff;
    }


    private void setHumidityRange(Range<Double> humidityRange) {
        if (humidityRange.getMinimum() >= 0 && humidityRange.getMaximum() <= 1.00)
            this.humidityRange = humidityRange;
        else
            this.humidityRange = Range.between(0.00, 0.50);
    }


    public void writeOffByReason(String reason, long userId) {
        //ВТОРОЕ СПИСАНИЕ
        if (this.writtenOff == null) {
            this.writtenOff = new WrittenOff(userId, this, reason);
        }
    }

    public void writeOffByExpDate(long userId) {
        if (this.writtenOff == null) {
            if (expDate.before(new Date())) {
                this.writtenOff = new WrittenOff(userId, this, "Expiration date");
                this.writtenOff.approve();
            }
        }
    }

    public void forSale(int amount, long invokerId, long recieverId) {
        if (amount > 0 && amount <= getPlacedAmount()) {
            this.operations.get(Operations.ForSale).add(new WorkRecord(this, amount, invokerId, recieverId));
        }
    }

    public void backFromSale(int amount, long invokerId) {
        if (amount > 0 && amount <= getOnSaleAmount()) {
            this.operations.get(Operations.Returned).add(new WorkRecord(this, amount, invokerId));
            this.returnedAmount = this.returnedAmount + amount;
        }
    }

    //если есть возвращенные товары и происходит размещение, в первую очередь произволится размещение возвращенных товаров

    // возвращать созданную операцию
    public void placing(Section section, int amount, long invokerId) {
        if (amount > 0 && amount <= getUnplacedAmount()) {
            if (amount < this.returnedAmount)
                this.returnedAmount = this.returnedAmount - amount;
            else
                this.returnedAmount = 0;
            this.operations.get(Operations.Incoming).add(new WorkRecord(this, section, amount, invokerId));
        }
    }

    public int getUnplacedAmount() {
        return this.amount - getPlacedAmount() - getOnSaleAmount();
    }

    public int getOnSaleAmount() {
        int onSaleSum = this.operations.get(Operations.ForSale).stream().mapToInt(WorkRecord::getAmount).sum();
        int returnedSum = this.operations.get(Operations.Returned).stream().mapToInt(WorkRecord::getAmount).sum();
        return onSaleSum - returnedSum;
    }

    public int getPlacedAmount() {
        int incomingSum = this.operations.get(Operations.Incoming).stream().mapToInt(WorkRecord::getAmount).sum();
        int onSaleSum = this.operations.get(Operations.ForSale).stream().mapToInt(WorkRecord::getAmount).sum();
        return incomingSum - onSaleSum;
    }

    public HashMap<Section, Integer> getAmountBySection() {
        Set<Section> sections = this.operations.get(Operations.Incoming).stream()
                .map(WorkRecord::getSection)
                .collect(Collectors.toSet());
        List<WorkRecord> incomingWorkRecords = this.operations.get(Operations.Incoming);
        List<WorkRecord> forSaleWorkRecords = this.operations.get(Operations.ForSale);
        HashMap<Section, Integer> result = new HashMap<>();
        for (Section section : sections) {
            int incomeForSectionCount = incomingWorkRecords.stream().filter(workRecord ->
                    workRecord.getSection().equals(section)).mapToInt(WorkRecord::getAmount).sum();
            int outComeForSectionCount = forSaleWorkRecords.stream().filter(workRecord ->
                    workRecord.getSection().equals(section)).mapToInt(WorkRecord::getAmount).sum();
            result.put(section, incomeForSectionCount - outComeForSectionCount);
        }
        return result;
    }

    public int getReturnedAmount() {
        return this.returnedAmount;
    }

    public void setHardOperations(Map<Operations, List<WorkRecord>> ops){
        operations = ops;
    }

    public void setHardWrittenOff(WrittenOff wo){
        this.writtenOff = wo;
    }

    public void setHardSupply(Supply s){
        this.supply = s;
    }
}
