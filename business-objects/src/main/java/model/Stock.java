package model;

import java.util.LinkedList;
import java.util.List;

public class Stock {
    private int number;
    private String address;
    private LinkedList<Section> sections;

    public Stock(int number, String address) {
        setNumber(number);
        this.address = address;
        this.sections = new LinkedList<>();
    }

    public int getNumber() {
        return number;
    }

    public String getAddress() {
        return address;
    }
    public LinkedList<Section> getSections() {
        return sections;
    }

    private void setNumber(int number) {
        if (number > 0)
            this.number = number;
        else
            this.number = 0;
    }

    public Section getSectionByNumber(int number){
        for (Section sec : sections){
            if (sec.getNumber() == number)
                return sec;
        }
        throw new RuntimeException("Section not found");
    }

}
