package model;

import java.util.Date;

public class WrittenOff {
    private long userId;
    private Consignment consignment;
    private String reason;
    private boolean approved;
    private Date date;

    public WrittenOff(long userId, Consignment consignment, String reason, boolean isApproved, Date date) {
        this.userId = userId;
        this.consignment = consignment;
        this.reason = reason;
        this.approved = isApproved;
        this.date = date;
    }

    public WrittenOff(long userId, Consignment consignment, String reason) {
        this.userId = userId;
        this.consignment = consignment;
        this.reason = reason;
        this.approved = false;
    }

    public void approve() {
        if(this.date == null) {
            this.approved = true;
            this.date = new Date();
        }
    }

    public boolean isApproved() {
        return approved;
    }

    public Date getDate() {
        return date;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Consignment getConsignment() {
        return consignment;
    }

    public void setConsignment(Consignment consignment) {
        this.consignment = consignment;
    }
}
