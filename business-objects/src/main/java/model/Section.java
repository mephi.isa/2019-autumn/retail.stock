package model;

import java.util.Objects;

public class Section {
    private int number;
    private int maxAmount;
    private Stock stock;
    private int temperature;
    private double humidity;

    public Section(int number, int maxAmount, Stock stock, int temperature, double humidity) {
        this.number = number;
        setMaxAmount(maxAmount);
        this.stock = stock;
        this.temperature = temperature;
        this.humidity = humidity;
    }

    public int getMaxAmount() {
        return maxAmount;
    }
    /*  public Stock getStock() {
          return stock;
      }*/
    public int getTemperature() {
        return temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setMaxAmount(int amount) {
        if (amount > 0)
            this.maxAmount = amount;
        else
            this.maxAmount = 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Section section = (Section) o;
        return maxAmount == section.maxAmount &&
                temperature == section.temperature &&
                Double.compare(section.humidity, humidity) == 0 &&
                Objects.equals(stock, section.stock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(maxAmount, stock, temperature, humidity);
    }

    public Stock getStock(){
        return stock;
    }

    public void setStock(Stock stock){this.stock = stock;}

    public int getNumber() {
        return number;
    }
}
