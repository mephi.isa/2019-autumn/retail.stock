package model;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Supply {
    private int number;
    private LinkedList<Consignment> consignments;
    private String provider;
    private Date date;
    private Status status;

    public Supply(int number, String provider, Date date, Status status){
        this.number = number; this.provider = provider; this.date = date; this.status = status;
    }

    public int getNumber() {
        return number;
    }

    public LinkedList<Consignment> getConsignments() {
        return consignments;
    }

    public void addConsignment(Consignment cons){
        if (consignments == null) consignments = new LinkedList<Consignment>();
        consignments.add(cons);
    }

    public String getProvider() {
        return provider;
    }

    public Date getDate() {
        return date;
    }

    public Status getStatus() {
        return status;
    }

    public Supply(int number, String provider, Date date) {
        setNumber(number);
        this.provider = provider;
        this.date = date;
        this.status = new Status(this);
        this.consignments = consignments;
    }

    public void setNumber(int number) {
        if (number > 0)
            this.number = number;
        else
            this.number = 0;
    }

    public void approve() {
        if (!this.status.isApproved())
            this.status.setApproved(true);
    }

    public void decline(String description) {
        if (!this.status.isDeclined()) {
            this.status.setDeclined(true);
            this.status.setDescription(description);
        }
    }
}
