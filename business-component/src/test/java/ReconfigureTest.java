
import model.*;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;


public class ReconfigureTest {
    DataManager dataManager = new DataManager(
            new StockListContainer(new Stock(1, "qqq")),
            new SupplyListContainer(),
            new SupplyRejectionRequestsContainer());
    UserInterfaceEmulator ui = new UserInterfaceEmulator();
    SupplyPlanningSystemInterfaceEmulator spi = new SupplyPlanningSystemInterfaceEmulator();

    Reconfigure reconfigure = new Reconfigure(dataManager, ui);

    @Test
    void section_added_approved() throws IOException {
        String action = "add_section";
        ui.setAnswer("sectionAddApproveDialog", "true");
        boolean res = reconfigure.reconfigure(action, 1);
        assertTrue(res);

        assertEquals(1, dataManager.stocks().get(1).getSections().size());
        Section resSec = dataManager.stocks().get(1).getSections().get(0);
        Section expected = new Section(1, 100, dataManager.stocks().get(1), 10, 0.4);
        assertTrue(resSec.getMaxAmount() == expected.getMaxAmount() &&
                resSec.getHumidity() == expected.getHumidity() &&
                resSec.getTemperature() == expected.getTemperature() &&
                resSec.getStock() == expected.getStock());
    }

    @Test
    void section_added_declined() throws IOException {
        String action = "add_section";
        ui.setAnswer("sectionAddApproveDialog", "false");
        reconfigure.reconfigure(action, 1);
        boolean res = reconfigure.reconfigure(action, 1);
        assertFalse(res);

        assertEquals(0, dataManager.stocks().get(1).getSections().size());
    }

    @Test
    void section_params_changed() throws IOException {
        dataManager.stocks().get(1).getSections().add(
                new Section(2, 50, dataManager.stocks().get(1), 0, 0.1)
        );

        String action = "change_section";
        ui.setAnswer("sectionModifyApproveDialog", "true");
        boolean res = reconfigure.reconfigure(action, 1);
        assertTrue(res);

        assertEquals(1, dataManager.stocks().get(1).getSections().size());
        Section resSec = dataManager.stocks().get(1).getSections().get(0);
        Section expected = new Section(1, 100, dataManager.stocks().get(1), 10, 0.4);
        assertTrue(resSec.getMaxAmount() == expected.getMaxAmount() &&
                resSec.getHumidity() == expected.getHumidity() &&
                resSec.getTemperature() == expected.getTemperature() &&
                resSec.getStock() == expected.getStock());
    }

    @Test
    void section_deleted() throws IOException {
        dataManager.stocks().get(1).getSections().add(
                new Section(2, 50, dataManager.stocks().get(1), 0, 0.1)
        );

        String action = "delete_section";
        ui.setAnswer("sectionDeleteApproveDialog", "true");
        boolean res = reconfigure.reconfigure(action, 1);
        assertTrue(res);

        assertEquals(0, dataManager.stocks().get(1).getSections().size());
    }
}
