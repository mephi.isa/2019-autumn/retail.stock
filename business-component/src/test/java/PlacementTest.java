import model.*;

import org.apache.commons.lang3.Range;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

public class PlacementTest {
    DataManager dataManager = new DataManager(
            new StockListContainer(new Stock(1, "qqq")),
            new SupplyListContainer(),
            new SupplyRejectionRequestsContainer());
    UserInterfaceEmulator ui = new UserInterfaceEmulator();
    SupplyPlanningSystemInterfaceEmulator spi = new SupplyPlanningSystemInterfaceEmulator();

    Placement placement = new Placement(dataManager, ui);

    @Test
    void not_enough_empty_space_and_goods_werent_placed() throws IOException {
        Supply supply1 = new Supply(1, "provider1", new Date());
        Consignment cons1 = new Consignment(
                "art1",
                new Date(2020, Calendar.JANUARY, 1),
                100,
                supply1,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        supply1.addConsignment(cons1);

        Section sec1 = new Section(1, 10, dataManager.stocks().get(1), 0, 0.5);

        dataManager.stocks().get(1).getSections().add(sec1);
        dataManager.supplies().addSupply(supply1);

        ui.setAnswer("howManyGoodsPlacedDialog", "0");

        LinkedList<Consignment> unplaced = dataManager.supplies().getConsignmentsWithUnplacedGoods();
        int res = placement.place(1, unplaced);

        assertEquals(0, res);
        assertEquals(100, dataManager.supplies().getConsignmentsWithUnplacedGoods().get(0).getUnplacedAmount());
    }

    @Test
    void not_enough_empty_space_and_goods_were_placed_and_system_approved() throws IOException {
        Supply supply1 = new Supply(1, "provider1", new Date());
        Consignment cons1 = new Consignment(
                "art1",
                new Date(2020, Calendar.JANUARY, 1),
                10,
                supply1,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        supply1.addConsignment(cons1);

        Section sec1 = new Section(1, 20, dataManager.stocks().get(1), 0, 0.5);

        dataManager.stocks().get(1).getSections().add(sec1);
        dataManager.supplies().addSupply(supply1);

        ui.setAnswer("howManyGoodsPlacedDialog", "10");

        LinkedList<Consignment> unplaced = dataManager.supplies().getConsignmentsWithUnplacedGoods();
        int res = placement.place(1, unplaced);

        assertEquals(10, res);
        assertEquals(0, dataManager.supplies().getConsignmentsWithUnplacedGoods().size());
        // TODO: Recommended to use getConsignmentsBysection
    }

    @Test
    void enough_empty_space() throws IOException {
        Supply supply1 = new Supply(1, "provider1", new Date());
        Consignment cons1 = new Consignment(
                "art1",
                new Date(2020, Calendar.JANUARY, 1),
                10,
                supply1,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        supply1.addConsignment(cons1);

        Section sec1 = new Section(1, 100, dataManager.stocks().get(1), 0, 0.5);

        dataManager.stocks().get(1).getSections().add(sec1);
        dataManager.supplies().addSupply(supply1);

        ui.setAnswer("howManyGoodsPlacedDialog", "10");

        LinkedList<Consignment> unplaced = dataManager.supplies().getConsignmentsWithUnplacedGoods();
        int res = placement.place(1, unplaced);

        assertNotEquals(0, res);
        assertEquals(0, dataManager.supplies().getConsignmentsWithUnplacedGoods().size());
    }
}