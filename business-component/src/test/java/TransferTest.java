import model.*;

import org.apache.commons.lang3.Range;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

public class TransferTest {
    DataManager dataManager = new DataManager(
            new StockListContainer(new Stock(1, "qqq")),
            new SupplyListContainer(),
            new SupplyRejectionRequestsContainer());
    UserInterfaceEmulator ui = new UserInterfaceEmulator();
    SupplyPlanningSystemInterfaceEmulator spi = new SupplyPlanningSystemInterfaceEmulator();

    Transfer transfer = new Transfer(dataManager, ui);

    void initData() throws IOException {
        Supply supply1 = new Supply(1, "provider1", new Date());
        Consignment cons1 = new Consignment(
                "art1",
                new Date(2020, Calendar.JANUARY, 1),
                10,
                supply1,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        Consignment cons2 = new Consignment(
                "art2",
                new Date(2020, Calendar.JANUARY, 1),
                20,
                supply1,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        supply1.addConsignment(cons1);
        supply1.addConsignment(cons2);

        dataManager.supplies().addSupply(supply1);

        Section sec1 = new Section(1, 100, dataManager.stocks().get(1), 0, 0.5);
        dataManager.stocks().get(1).getSections().add(sec1);

        cons1.placing(sec1, 10, 1);
        cons2.placing(sec1, 20, 1);
    }

    @Test
    void cons_in_list_amount_is_enough_list_empty() throws IOException {
        initData();

        ui.setAnswer("continueTransferDialog", "false");
        ui.setAnswer("whichAmountToTransfer", "1");
        ui.setAnswer("consignmentToTransferChoiceDialog", "art1");

        assertTrue(transfer.transfer(1 , 2));
        LinkedList<Consignment> onSale = dataManager.supplies().getConsignmentsOnSale();

        assertEquals(1, onSale.size());
        assertEquals(1, onSale.get(0).getOnSaleAmount());
    }

    @Test
    void cons_in_list_amount_is_not_enough_list_empty() throws IOException {
        initData();

        ui.setAnswer("continueTransferDialog", "false");
        ui.setAnswer("whichAmountToTransfer", "100");
        ui.setAnswer("consignmentToTransferChoiceDialog", "art1");

        assertTrue(transfer.transfer(1 , 2));
        LinkedList<Consignment> onSale = dataManager.supplies().getConsignmentsOnSale();

        assertEquals(1, onSale.size());
        assertEquals(10, onSale.get(0).getOnSaleAmount());
    }

    @Test
    void cons_not_in_list_list_empty() throws IOException {
        initData();

        ui.setAnswer("continueTransferDialog", "false");
        ui.setAnswer("whichAmountToTransfer", "100");
        ui.setAnswer("consignmentToTransferChoiceDialog", "non-existing-art");

        assertFalse(transfer.transfer(1 , 2));
        LinkedList<Consignment> onSale = dataManager.supplies().getConsignmentsOnSale();

        assertEquals(0, onSale.size());
    }

}