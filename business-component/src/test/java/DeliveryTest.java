import model.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.HashMap;

public class DeliveryTest {
    DataManager dataManager = new DataManager(
            new StockListContainer(new Stock(1, "qqq")),
            new SupplyListContainer(),
            new SupplyRejectionRequestsContainer());

    UserInterfaceEmulator ui = new UserInterfaceEmulator();
    SupplyPlanningSystemInterfaceEmulator spi = new SupplyPlanningSystemInterfaceEmulator();

    Delivery del = new Delivery(dataManager, ui, spi);

    public DeliveryTest(){
        try {
            dataManager.stocks().get(1).getSections().add(
                    new Section(1, 20, dataManager.stocks().get(1), 0, 0.5));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    void test2_goods_are_ok_placement_successful() throws IOException {
        int tgtSupply = ui.supplyChoiceDialog(spi.getTodaySupplies());
        for (Supply sup : spi.getTodaySupplies()) {
            dataManager.supplies().addSupply(sup);
        }
        HashMap<String, Boolean> res =  del.delivery(1, true);

        HashMap<String, Boolean>expected_res = new HashMap<String, Boolean>();
        expected_res.put("rejection_request_created", false);
        expected_res.put("goods_accepted", true);
        expected_res.put("all_goods_placed", false);

        assertEquals(expected_res, res);
    }

    @Test
    void test3_goods_are_ok_placement_unsuccessful() throws IOException {
        ui.setAnswer("howManyGoodsPlacedDialog", "0");
        int tgtSupply = ui.supplyChoiceDialog(spi.getTodaySupplies());
        for (Supply sup : spi.getTodaySupplies()) {
            dataManager.supplies().addSupply(sup);
        }

        HashMap<String, Boolean> res =  del.delivery(1, true);

        HashMap<String, Boolean>expected_res = new HashMap<String, Boolean>();
        expected_res.put("rejection_request_created", false);
        expected_res.put("goods_accepted", true);
        expected_res.put("all_goods_placed", false);

        assertEquals(expected_res, res);

        ui.resetAnswerMap();
    }

    @Test
    void test4_goods_are_not_ok_user_is_manager() throws IOException {
        ui.setAnswer("areGoodsInSupplyOkDialog", "false");
        int tgtSupply = ui.supplyChoiceDialog(spi.getTodaySupplies());
        for (Supply sup : spi.getTodaySupplies()) {
            dataManager.supplies().addSupply(sup);
        }
        Supply supply = dataManager.supplies().getSupplyByNumber(tgtSupply);
        HashMap<String, Boolean> res =  del.delivery(1, true);

        HashMap<String, Boolean>expected_res = new HashMap<String, Boolean>();
        expected_res.put("rejection_request_created", false);
        expected_res.put("goods_accepted", false);
        expected_res.put("all_goods_placed", false);

        assertEquals(expected_res, res);

        ui.resetAnswerMap();
    }

    @Test
    void test5_goods_are_not_ok_user_is_not_manager() throws IOException {
        ui.setAnswer("areGoodsInSupplyOkDialog", "false");
        int tgtSupply = ui.supplyChoiceDialog(spi.getTodaySupplies());
        for (Supply sup : spi.getTodaySupplies()) {
            dataManager.supplies().addSupply(sup);
        }
        HashMap<String, Boolean> res =  del.delivery(1, false);

        HashMap<String, Boolean>expected_res = new HashMap<String, Boolean>();
        expected_res.put("rejection_request_created", true);
        expected_res.put("goods_accepted", false);
        expected_res.put("all_goods_placed", false);

        SupplyRejectionRequest srr = dataManager.supplyRejectionRequests().getRequests().get(0);

        assertEquals(expected_res, res);
        assertFalse(srr.getIsProcessed());
        assertEquals(spi.getTodaySupplies().get(0).getNumber(), srr.getSupplyNumber());

        ui.resetAnswerMap();
    }

    @Test
    void test6_reject_rejection_request() throws IOException {
        ui.setAnswer("areGoodsInSupplyOkDialog", "false");
        int tgtSupply = ui.supplyChoiceDialog(spi.getTodaySupplies());
        for (Supply sup : spi.getTodaySupplies()) {
            dataManager.supplies().addSupply(sup);
        }
        HashMap<String, Boolean> res =  del.delivery(1, false);

        ui.setAnswer("approveSupplyRejectionRequest", "false");

        SupplyRejectionRequest tgt = null;
        int tgtNum = 0;
        try {
            tgtNum = ui.chooseSupplyRejectionRequest(dataManager.supplyRejectionRequests().getRequests());
            tgt = dataManager.supplyRejectionRequests().getRequestBySupplyNumber(tgtNum);
        } catch (IOException e) {
            e.printStackTrace();
        }
        del.supplyRejectionRequestsProcessing(true, tgtNum, ui.approveSupplyRejectionRequest(
                dataManager.supplies().getSupplyByNumber(tgt.getSupplyNumber())
        ));

        assertTrue(tgt.getIsProcessed());
        assertTrue(dataManager.supplies().getSupplyByNumber(tgt.getSupplyNumber()).getStatus().isApproved());
        assertFalse(dataManager.supplies().getSupplyByNumber(tgt.getSupplyNumber()).getStatus().isDeclined());
        assertFalse(tgt.getIsArchived());

        ui.resetAnswerMap();
    }

    @Test
    void test7_goods_after_rejection_request_placed() throws IOException {
        ui.setAnswer("areGoodsInSupplyOkDialog", "false");

        int tgtSupply = ui.supplyChoiceDialog(spi.getTodaySupplies());
        for (Supply sup : spi.getTodaySupplies()) {
            dataManager.supplies().addSupply(sup);
        }
        del.delivery(1, false);

        SupplyRejectionRequest srr = null;
        try {
            srr = dataManager.supplyRejectionRequests().getRequests().get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertFalse(srr.getIsProcessed());

        ui.setAnswer("approveSupplyRejectionRequest", "false");
        int tgtNum = ui.chooseSupplyRejectionRequest(dataManager.supplyRejectionRequests().getRequests());
        SupplyRejectionRequest tgt = dataManager.supplyRejectionRequests().getRequestBySupplyNumber(tgtNum);
        del.supplyRejectionRequestsProcessing(true, tgtNum,
                ui.approveSupplyRejectionRequest(dataManager.supplies().getSupplyByNumber(tgt.getSupplyNumber())));

        boolean res = del.processSupplyFromRejectionRequests(1, tgtNum);

        assertFalse(res);
        assertTrue(srr.getIsArchived());
    }

}
