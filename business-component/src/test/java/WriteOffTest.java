import model.*;

import org.apache.commons.lang3.Range;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

public class WriteOffTest {
    DataManager dataManager = new DataManager(
            new StockListContainer(new Stock(1, "qqq")),
            new SupplyListContainer(),
            new SupplyRejectionRequestsContainer());
    UserInterfaceEmulator ui = new UserInterfaceEmulator();
    SupplyPlanningSystemInterfaceEmulator spi = new SupplyPlanningSystemInterfaceEmulator();

    WriteOff wo = new WriteOff(dataManager, ui);

    void initData(){
        Supply supply1 = new Supply(1, "provider1", new Date());
        Consignment cons1 = new Consignment(
                "art1",
                new Date(120, Calendar.JANUARY, 1),
                10,
                supply1,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        Consignment cons2 = new Consignment(
                "art2",
                new Date(121, Calendar.FEBRUARY, 1),
                20,
                supply1,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        supply1.addConsignment(cons1);
        supply1.addConsignment(cons2);

        dataManager.supplies().addSupply(supply1);

        Section sec1 = null;
        try {
            sec1 = new Section(1, 100, dataManager.stocks().get(1), 0, 0.5);
            dataManager.stocks().get(1).getSections().add(sec1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        cons1.placing(sec1, 10, 1);
        cons2.placing(sec1, 20, 1);
    }

    @Test
    void writeoff_by_date_expiration(){
        initData();

        ui.setAnswer("consignmentChoiceDialog", "0");
        ui.setAnswer("writeOffReasonDialog", "Consignment expired");

        wo.writeOff(1, false);

        WrittenOff writtenOff = dataManager.supplies().getSupplyByNumber(1).getConsignments().get(0).getWrittenOff();
        assertNotNull(writtenOff);
        assertTrue(writtenOff.isApproved());
    }

    @Test
    void writeoff_another_reason_user_is_manager(){
        initData();

        ui.setAnswer("consignmentChoiceDialog", "0");
        ui.setAnswer("writeOffReasonDialog", "Some reason");

        wo.writeOff(1, true);

        WrittenOff writtenOff = dataManager.supplies().getSupplyByNumber(1).getConsignments().get(0).getWrittenOff();
        assertNotNull(writtenOff);
        assertTrue(writtenOff.isApproved());
    }

    @Test
    void writeoff_another_reason_user_is_not_manager_request_approved(){
        initData();

        ui.setAnswer("consignmentChoiceDialog", "0");
        ui.setAnswer("writeOffReasonDialog", "Some reason");

        wo.writeOff(1, false);

        WrittenOff writtenOff = dataManager.supplies().getConsignmentsWithWriteOffRequests().get(0).getWrittenOff();
        assertNotNull(writtenOff);
        assertFalse(writtenOff.isApproved());

        ui.setAnswer("writeOffApproveDialog", "true");
        wo.writeOffRequestProcess(2, true, "art1");
        assertTrue(writtenOff.isApproved());
    }

    @Test
    void writeoff_another_reason_user_is_not_manager_request_rejected(){
        initData();

        ui.setAnswer("consignmentChoiceDialog", "0");
        ui.setAnswer("writeOffReasonDialog", "Some reason");

        wo.writeOff(1, false);

        WrittenOff writtenOff = dataManager.supplies().getConsignmentsWithWriteOffRequests().get(0).getWrittenOff();
        assertNotNull(writtenOff);
        assertFalse(writtenOff.isApproved());

        ui.setAnswer("writeOffApproveDialog", "false");
        wo.writeOffRequestProcess(2, true, "art1");
        assertFalse(writtenOff.isApproved());
    }
}