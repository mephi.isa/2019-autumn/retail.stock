import model.*;

import org.apache.commons.lang3.Range;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

public class ReturnFromSaleTest {
    DataManager dataManager = new DataManager(
            new StockListContainer(new Stock(1, "qqq")),
            new SupplyListContainer(),
            new SupplyRejectionRequestsContainer());
    UserInterfaceEmulator ui = new UserInterfaceEmulator();
    SupplyPlanningSystemInterfaceEmulator spi = new SupplyPlanningSystemInterfaceEmulator();

    ReturnFromSale returnFromSale = new ReturnFromSale(dataManager, ui);

    @Test
    void loop_with_write_off() throws IOException {
        Supply supply1 = new Supply(1, "provider1", new Date());
        Consignment cons1 = new Consignment(
                "art1",
                new Date(2020, Calendar.JANUARY, 1),
                10,
                supply1,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        Consignment cons2 = new Consignment(
                "art2",
                new Date(2020, Calendar.JANUARY, 1),
                20,
                supply1,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        Consignment cons3 = new Consignment(
                "art3",
                new Date(2020, Calendar.JANUARY, 1),
                30,
                supply1,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        supply1.addConsignment(cons1);
        supply1.addConsignment(cons2);
        supply1.addConsignment(cons3);

        dataManager.supplies().addSupply(supply1);

        Section sec1 = new Section(1, 100, dataManager.stocks().get(1), 0, 0.5);
        dataManager.stocks().get(1).getSections().add(sec1);

        cons1.placing(sec1, 10, 1);
        cons1.forSale(5, 1, 2);
        cons2.placing(sec1, 20, 1);
        cons2.forSale(10, 1, 2);
        cons3.placing(sec1, 30, 1);

        LinkedList<Consignment> onSale = dataManager.supplies().getConsignmentsOnSale();
        assertEquals(2, onSale.size());

        ui.setAnswer("isReturnedConsignmentOk", "false");

        returnFromSale.returnFromSale(1);

        onSale = dataManager.supplies().getConsignmentsOnSale();

        assertEquals(0, onSale.size());
        for (Consignment cons : supply1.getConsignments().subList(0,2)){
            assertNotNull(cons.getWrittenOff());
        }
        Consignment cons = supply1.getConsignments().get(2);
        assertEquals(cons3.getArticle(), cons.getArticle());

    }

    @Test
    void loop_with_placement() throws IOException {
        Supply supply1 = new Supply(1, "provider1", new Date());
        Consignment cons1 = new Consignment(
                "art1",
                new Date(2020, Calendar.JANUARY, 1),
                10,
                supply1,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        Consignment cons2 = new Consignment(
                "art2",
                new Date(2020, Calendar.JANUARY, 1),
                20,
                supply1,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        Consignment cons3 = new Consignment(
                "art3",
                new Date(2020, Calendar.JANUARY, 1),
                30,
                supply1,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        supply1.addConsignment(cons1);
        supply1.addConsignment(cons2);
        supply1.addConsignment(cons3);

        dataManager.supplies().addSupply(supply1);

        Section sec1 = new Section(1, 100, dataManager.stocks().get(1), 0, 0.5);
        dataManager.stocks().get(1).getSections().add(sec1);

        cons1.placing(sec1, 10, 1);
        cons1.forSale(1, 1, 2);
        cons2.placing(sec1, 20, 1);
        cons2.forSale(1, 1, 2);
        cons3.placing(sec1, 30, 1);

        LinkedList<Consignment> onSale = dataManager.supplies().getConsignmentsOnSale();
        assertEquals(2, onSale.size());

        ui.setAnswer("isReturnedConsignmentOk", "true");

        returnFromSale.returnFromSale(1);

        onSale = dataManager.supplies().getConsignmentsOnSale();

        assertEquals(0, onSale.size());
        assertEquals(3, supply1.getConsignments().size());
        for (Consignment cons : supply1.getConsignments()){
            assertEquals(0, cons.getOnSaleAmount());
            assertEquals(0, cons.getUnplacedAmount());
        }
    }
}
