package model;

import java.util.LinkedList;

public class SupplyRejectionRequestsContainer implements SupplyRejectionRequestsInterface {
    LinkedList<SupplyRejectionRequest> requests;

    public SupplyRejectionRequestsContainer(){
        requests = new LinkedList<SupplyRejectionRequest>();
    }


    public Boolean createRequest(Supply supply, long userId){
        requests.add(new SupplyRejectionRequest(supply.getNumber(), userId, -1, false, false));
        return true;
    }

    public LinkedList<SupplyRejectionRequest> getRequests(){
        return requests;
    }

    public LinkedList<Integer> getSuppliesNumbersFromRequests(Boolean isArchived, Boolean isProcessed) {
        LinkedList<Integer> res = new LinkedList<Integer>();
        for (SupplyRejectionRequest item : requests)
            if (item.getIsArchived() == isArchived &&
                    item.getIsProcessed() == isProcessed)
                res.add(item.getSupplyNumber());
        return res;
    }

    public SupplyRejectionRequest getRequestBySupplyNumber(int number){
        for (SupplyRejectionRequest req : requests){
            if (req.getSupplyNumber() == number) return req;
        }
        return null;
    }

    public LinkedList<Integer> getPlaceableSuppliesNumbersFromRequests(){
        return getSuppliesNumbersFromRequests(false, true);
    }
}
