package model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {
    static private String pattern = "MM/dd/yyyy HH:mm:ss";

    static public String dateToString(Date date) {
        DateFormat df = new SimpleDateFormat(pattern);
        return df.format(date);
    }

    static public String dateToString(Date date, String format) {
        DateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    public static Date stringToDate(String in) throws ParseException {
        return new SimpleDateFormat(pattern).parse(in);
    }

    public static Date stringToDate(String in, String pattern) throws ParseException {
        return new SimpleDateFormat(pattern).parse(in);
    }
}
