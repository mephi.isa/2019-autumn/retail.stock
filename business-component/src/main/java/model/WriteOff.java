package model;

import java.util.Date;
import java.util.LinkedList;

public class WriteOff {
    private DataManager dataManager;
    private UserInterface ui;

    public WriteOff(DataManager dataManager, UserInterface ui){
        this.dataManager = dataManager;
        this.ui = ui;
    }

    public void writeOff(int userId, boolean userIsStockManager){
        LinkedList<Consignment> expiredConsignments = dataManager.supplies().getExpiredProductsByDate(new Date());
        Consignment tgtConsignment = ui.consignmentChoiceDialog(expiredConsignments);
        String reason = ui.writeOffReasonDialog(tgtConsignment);
        if (reason.equals("Consignment expired")) {
            tgtConsignment.writeOffByExpDate(userId);
            return;
        }
        tgtConsignment.writeOffByReason(reason, userId);
        if (userIsStockManager){
            tgtConsignment.getWrittenOff().approve();
        }
    }

    public void writeOffRequestProcess(int userId, boolean userIsStockManager, String artToProcess){
        if (! userIsStockManager){
            System.out.print("Insufficient rights to use writeOffRequstsProcess\n");
            return;
        }
        LinkedList<Consignment> consignments = dataManager.supplies().getConsignmentsWithWriteOffRequests();
        for(Consignment cons : consignments) {
            if (cons.getArticle().equals(artToProcess))
                if (ui.writeOffApproveDialog(cons))
                    cons.getWrittenOff().approve();
                break;
        }
    }

}
