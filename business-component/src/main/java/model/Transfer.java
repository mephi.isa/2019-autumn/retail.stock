package model;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.min;

public class Transfer {
    DataManager dataManager;
    UserInterface ui;

    public Transfer(DataManager d, UserInterface ui) {
        dataManager = d;
        this.ui = ui;
    }

    // TODO: функция получения списка артикулов -> в интерфейст БД
    private HashMap<Consignment, Integer> createTransferList(){
        HashMap<Consignment, HashMap<Section, Integer>> consignmentsList
                = dataManager.supplies().getConsignmentsListForTransfer();
        HashMap<Consignment, Integer> transferList = new HashMap<Consignment, Integer>();
        boolean continueTransferListCreation = true;
        while (continueTransferListCreation) {
            String tgtArt = ui.consignmentToTransferChoiceDialog(consignmentsList);
            Map.Entry<Consignment, HashMap<Section, Integer>> targetConsignmentEntry = null;
            for (Map.Entry<Consignment, HashMap<Section, Integer>> entry : consignmentsList.entrySet()){
                if (entry.getKey().getArticle().equals(tgtArt))
                    targetConsignmentEntry = entry;
            }
            if (targetConsignmentEntry != null) {  // Если "Нужный товар найден в списке"
                int amountToTransfer
                        = min(
                        targetConsignmentEntry.getKey().getAmount(),
                        ui.whichAmountToTransfer(targetConsignmentEntry.getKey())
                );
                transferList.put(targetConsignmentEntry.getKey(), amountToTransfer);
            }
            continueTransferListCreation = ui.continueTransferDialog();
        }
        return transferList;
    }

    public boolean transfer(long invokerId, long recieverId) {
        boolean goodsTransfered = false;
        HashMap<Consignment, Integer> transferList = createTransferList();
        for (Map.Entry<Consignment, Integer> entry : transferList.entrySet()){
            ui.consignmentSearchInfoDisplay(entry.getKey());
            entry.getKey().forSale(entry.getValue(), invokerId, recieverId);
            if (! goodsTransfered) goodsTransfered = true;
        }
        return goodsTransfered;
    }
}
