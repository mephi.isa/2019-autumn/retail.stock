package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.Range;


public class SupplyPlanningSystem implements SupplyPlanningSystemInterface {
    String serverUrl;
    String requestTemplate;

    static String dateFormat =
            "yyyy-MM-dd";
    // 2020-01-01

    static String filterResponse(String unfiltered){
        Pattern pattern = Pattern.compile("\\{.*\\}");
        Matcher m = pattern.matcher(unfiltered);
        m.find();
        return m.group(0).replace("&#34;", "\"");
    }

    public SupplyPlanningSystem(String serverIp) {
        serverUrl = "http://" + serverIp + ":3000/";
    }

    @Override
    public LinkedList<Supply> getTodaySupplies() throws IOException {
        String todayDate = DateConverter.dateToString(new Date(), dateFormat);
        String response = PostRequest.sendRequest(serverUrl,
                String.format("date_from=%s&date_to=%s",
                        todayDate, todayDate));

        String filteredResponse = filterResponse(response);

        JsonElement json = StringToJsonObject.convert(filteredResponse);

        LinkedList<Supply> result = new LinkedList<Supply>();
        // У Гатиловой есть API только на предоставление партий, которые планируется привезти в заданную дату.
        // Следовательно, информацию для сущности "Supply" брать неоткуда.
        // Следовательно, считаю, что все товары, которые должны прибыть сегодня, входят в одну поставку и генерирую поля number, provider
        // В качестве number беру один из "idPS" партий, возвращенных системой Гатиловой
        Integer newSupplyId = null;
        LinkedList<Consignment> consList = new LinkedList<>();
        for (JsonElement supplyEntry : json.getAsJsonObject().get("recordsets").getAsJsonArray().get(0).getAsJsonArray()){
            JsonObject objectEntry =  supplyEntry.getAsJsonObject();
            if (newSupplyId == null) newSupplyId = objectEntry.get("idPS").getAsInt();
            // В данных от Системы планирования поставок нет:
            // даты окончания срока годности продукта, данных о необходимой температуре и влажности.
            // заполняю их значениями по умолчанию
            try {
                consList.add(new Consignment(
                        objectEntry.get("article").getAsString(),
                        DateConverter.stringToDate("2021-10-10", dateFormat),
                        objectEntry.get("amount").getAsInt(), null,
                        Range.between(-10, 10), Range.between(0.1, 0.9)
                ));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        Supply newSup = new Supply(newSupplyId, "provider", new Date());

        for (Consignment cons : consList){
            cons.setHardSupply(newSup);
            newSup.addConsignment(cons);
        }

        result.add(newSup);

        return result;
    }

    @Override
    public boolean sendSupplyAcceptState(boolean isAccepted, Supply supply) {
        return false;
    }

    @Override
    public boolean sendConsignmentRejectedMessage(Consignment cons) {
        return false;
    }

    public static void main(String[] args) throws IOException {
        SupplyPlanningSystem sps = new SupplyPlanningSystem("192.168.56.101");
        sps.getTodaySupplies();
    }
}
