package model;

public interface PersonelSystem {
    public int getIdByLogin (String login);
    boolean userIsManager(int id);
}
