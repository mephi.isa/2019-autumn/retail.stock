package model;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.util.List;

public class SupplyChoiceDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonChoose;
    private JButton buttonCancel;
    private JTable table1;
    StringBuilder result;

    public SupplyChoiceDialog(String[] cols, List<Object[]> rowsData, StringBuilder result) {
        this.result = result;

        DefaultTableModel tableModel = new DefaultTableModel(cols, 0);
        table1.setModel(tableModel);
        for (Object[] entry : rowsData){
            tableModel.addRow(entry);
        }

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonChoose);

        buttonChoose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        result.append(table1.getValueAt(table1.getSelectedRow(), 0));
        dispose();
    }

    private void onCancel() {
        result.append("\0");
        dispose();
    }
}
