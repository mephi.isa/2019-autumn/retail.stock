package model;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class DbSaver {
    private String dbName;

    DbSaver(String dbName) throws SQLException, IOException {
        this.dbName = dbName;
        SqliteDbInterace.createStockTables(dbName);
    }


    public boolean saveStock(Stock stock,
                             LinkedList<Supply> suppliesList,
                             LinkedList<SupplyRejectionRequest> rejectionRequests) throws SQLException {
        saveStock(stock);
        for (Supply sup : suppliesList) {
            saveSupply(sup);
        }
        for (SupplyRejectionRequest srr : rejectionRequests) {
            saveRequest(srr);
        }
        return true;
    }

    private boolean saveStock(Stock stock) throws SQLException {
        SqliteDbInterace.performUpdateQuery(dbName,
                String.format("insert into Stock (number, address)  \n" +
                        "values (%d, \"%s\");", stock.getNumber(), stock.getAddress()));

        for (Section sec : stock.getSections()) {
            saveSection(sec);
            linkStockSection(stock.getNumber(), sec.getNumber());
        }
        return true;
    }

    private boolean linkStockSection(int stockNum, int sectionNum) throws SQLException {
        SqliteDbInterace.performUpdateQuery(dbName,
                String.format("insert into SectionSet (stockNumber, sectionId) \n" +
                        "values (%d, %d);", stockNum, sectionNum));
        return true;
    }

    private boolean saveSection(Section sec) throws SQLException {

        SqliteDbInterace.performUpdateQuery(dbName,
                String.format("insert into Section (number, maxAmount, temperature, humidity) \n" +
                        "values (%d, %d, %d, %f);", sec.getNumber(), sec.getMaxAmount(), sec.getTemperature(), sec.getHumidity()));
        return true;
    }

    private boolean saveConsignment(Consignment cons) throws SQLException {
        boolean writtenOffAccessible = ! (cons.getWrittenOff() == null);
        SqliteDbInterace.performUpdateQuery(dbName,
                String.format("insert into Consignment " +
                                "(article, expDate, amount, " +
                                "returnedAmount, tempRangeMax, tempRangeMin, " +
                                "humidityRangeMax, humidityRangeMin," +
                                "wo_userId, wo_reason, wo_approved, wo_date)  \n" +
                                "values (\"%s\", \"%s\", %d, " +
                                "%d, %d, %d, " +
                                "%f, %f," +
                                "$s, %s, %s, %s);",
                        cons.getArticle(), cons.getExpDate().toString(), cons.getAmount(),
                        cons.getReturnedAmount(), cons.getTemperatureRange().getMaximum(), cons.getTemperatureRange().getMinimum(),
                        cons.getHumidityRange().getMaximum(), cons.getHumidityRange().getMaximum(),
                        writtenOffAccessible ?
                                "\"" + cons.getWrittenOff().getUserId() + "\"" : "NULL",
                        writtenOffAccessible ?
                                "\"" + cons.getWrittenOff().getReason() + "\"" : "NULL",
                        writtenOffAccessible ?
                                (cons.getWrittenOff().isApproved() ? "\"1\"" : "\"0\"") : "NULL",
                        writtenOffAccessible ?
                                "\"" + DateConverter.dateToString(cons.getWrittenOff().getDate()) + "\"" : "NULL"));
        for (Map.Entry<Operations, List<WorkRecord>> entry : cons.getOperations().entrySet()) {
            for (WorkRecord op : entry.getValue()) {
                int res = saveOperation(entry.getKey(), op);
                linkConsignmentOperation(cons.getArticle(), res);
            }
        }
        return true;
    }

    private boolean saveSupply(Supply newSupply) throws SQLException {
        String str = String.format("insert into Supply (number, provider, date, isDeclined, isApproved, statusDescription)   \n" +
                        "values (%d, \"%s\", \"%s\", %d, %d, \"%s\");",
                newSupply.getNumber(),
                newSupply.getProvider(),
                DateConverter.dateToString(newSupply.getDate()),
                newSupply.getStatus().isDeclined() ? 1 : 0,
                newSupply.getStatus().isApproved() ? 1 : 0,
                newSupply.getStatus().getDescription());
        SqliteDbInterace.performUpdateQuery(dbName,
                str
        );
        for (Consignment cons : newSupply.getConsignments()) {
            saveConsignment(cons);
            linkSupplyConsignment(newSupply.getNumber(), cons.getArticle());
        }
        return true;
    }

    private boolean linkSupplyConsignment(int supply, String consArticle) throws SQLException {
        SqliteDbInterace.performUpdateQuery(dbName,
                String.format("insert into ConsignmentInSupply (supplyNumber, consignmentArticle)  \n" +
                        "values (%d, \"%s\");", supply, consArticle));
        return true;
    }

    private int saveOperation(Operations type, WorkRecord info) throws SQLException {
        SqliteDbInterace.performUpdateQuery(dbName,
                String.format("insert into Operation (type, sectionId, amount, invokerId, recieverId, date)   \n" +
                                "values (%d, %d, %d, %d, %d, \"%s\");",
                        type.ordinal(),
                        info.getSection().getNumber(),
                        info.getAmount(),
                        info.getUserInvokerId(),
                        info.getUserRecieverId(),
                        info.getDate().toString())
        );
        ResultSet rs = SqliteDbInterace.performSelectQuery(dbName, "select id from Operation order by id desc limit 1;");
        int res = rs.getInt("id");
        return res;
    }

    private boolean linkConsignmentOperation(String consArticle, int op) throws SQLException {
        SqliteDbInterace.performUpdateQuery(dbName,
                String.format("insert into OperationsSet (consignmentArticle, operationId)  \n" +
                        "values (\"%s\", %d);", consArticle, op));
        return true;
    }

    private Boolean saveRequest(SupplyRejectionRequest srr) throws SQLException {
        SqliteDbInterace.performUpdateQuery(dbName,
                String.format("insert into SupplyRejectionRequest (supplyId, creatorId, processorId, isProcessed, isArchived) \n" +
                                "values (%d, %d, %d, %d, %d);", srr.getSupplyNumber(), srr.getCreatorId(),
                        srr.getProcessorId(), (srr.getIsProcessed()) ? 1 : 0, (srr.getIsArchived()) ? 1 : 0));
        return true;
    }
}
