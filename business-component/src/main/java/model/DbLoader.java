package model;

import org.apache.commons.lang3.Range;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static model.Operations.*;

public class DbLoader {
    private String dbName;

    DbLoader(String dbName) throws SQLException, IOException {
        this.dbName = dbName;
        SqliteDbInterace.createStockTables(dbName);
    }



    public Stock loadStock(int number) throws IOException {
        Stock res;
        try {
            ResultSet rs = SqliteDbInterace.performSelectQuery(dbName,
                    String.format("select * from Stock \n" +
                            "where number = %d;", number));
            res = new Stock(number, rs.getString("address"));
            res.getSections().addAll(getSectionsForStock(res));
        } catch (IOException | SQLException e) {
            throw new IOException(e);
        }
        return res;
    }

    private LinkedList<Section> getSectionsForStock(Stock stock) throws IOException {
        LinkedList<Section> res = new LinkedList<Section>();
        try {
            ResultSet rs = SqliteDbInterace.performSelectQuery(dbName,
                    String.format("select * from Section " +
                            "where id in (" +
                            "select sectionId from SectionSet " +
                            "where stockNumber = %d" +
                            ");", stock.getNumber()));
            while (rs.next()) {
                res.add(new Section(
                        rs.getInt("number"),
                        rs.getInt("maxAmount"),
                        stock,
                        rs.getInt("temperature"),
                        rs.getDouble("humidity")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IOException("Query failed");
        }
        return res;
    }

    public LinkedList<Supply> loadSupplies(Stock stock) throws IOException {
        LinkedList<Supply> res = new LinkedList<Supply>();
        try {
            ResultSet rs = SqliteDbInterace.performSelectQuery(dbName,
                    "select * from Supply");
            while (rs.next()) {
                Status st = new Status(
                        rs.getInt("isDeclined") == 1,
                        rs.getInt("isApproved") == 1,
                        rs.getString("statusDescription"));
                Supply sup = new Supply(
                        rs.getInt("number"),
                        rs.getString("provider"),
                        DateConverter.stringToDate(rs.getString("date")),
                        st);
                st.setSupply(sup);
                LinkedList<Consignment> consignments = loadConsignmentsForSupply(sup, stock.getSections());
            }
        } catch (SQLException | IOException | ParseException e) {
            e.printStackTrace();
            throw new IOException("Query failed");
        }
        return res;
    }

    LinkedList<Consignment> loadConsignmentsForSupply(Supply supply, LinkedList<Section> sections) throws IOException {
        LinkedList<Consignment> res = new LinkedList<Consignment>();
        try {
            ResultSet rs = SqliteDbInterace.performSelectQuery(dbName,
                    String.format("select * from Consignment " +
                            "where article in (" +
                            "select consignmentArticle from consignmentInSupply " +
                            "where supplyNumber = %d);", supply.getNumber()));
            while (rs.next()) {
                Consignment tmp = new Consignment(
                        rs.getString("article"),
                        rs.getDate("expDate"),
                        rs.getInt("amount"),
                        supply,
                        Range.between(rs.getInt("tempRangeMin"), rs.getInt("tempRangeMax")),
                        Range.between(rs.getDouble("humidityRangeMin"), rs.getDouble("humidityRangeMax")),
                        null,
                        rs.getInt("returnedAmount")
                );
                WrittenOff wo = null;
                if (! rs.getString("wo_reason").equals("NULL")) {
                    wo = new WrittenOff(
                            rs.getInt("wo_userId"),
                            tmp,
                            rs.getString("wo_reason"),
                            rs.getInt("wo_approved") == 1,
                            DateConverter.stringToDate(rs.getString("wo_date")));
                }
                tmp.setHardWrittenOff(wo);
                tmp.setHardOperations(loadOperationsForConsignment(rs.getString("article"), tmp, sections));
                res.add(tmp);
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
            throw new IOException("Query failed");
        }
        return res;
    }

    Map<Operations, List<WorkRecord>> loadOperationsForConsignment(String consArticle, Consignment cons, LinkedList<Section> sections) throws SQLException {
        Map<Operations, List<WorkRecord>> res = new HashMap<Operations, List<WorkRecord>>();
        ResultSet rs = SqliteDbInterace.performSelectQuery(dbName,
                String.format("select * from Operation " +
                        "where id in (" +
                        "select operationId from operationsSet " +
                        "where consignmentArticle = %s)" +
                        "order by type desc;", consArticle));

        LinkedList<WorkRecord> list1 = new LinkedList<WorkRecord>();
        LinkedList<WorkRecord> list2 = new LinkedList<WorkRecord>();
        LinkedList<WorkRecord> list3 = new LinkedList<WorkRecord>();

        while (rs.next()) {
            WorkRecord temp = null;
            try {
                temp = new WorkRecord(
                        cons,
                        findInListByNumber(sections, rs.getInt("sectionId")),
                        rs.getInt("amount"),
                        rs.getInt("invokerId"),
                        rs.getInt("recieverId"),
                        DateConverter.stringToDate(rs.getString("date")));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            switch (Operations.valueOf(rs.getString("type"))) {
                case ForSale:
                    list1.add(temp);
                    break;
                case Incoming:
                    list2.add(temp);
                    break;
                case Returned:
                    list3.add(temp);
                    break;
                default:
                    break;
            }
        }
        res.put(ForSale, list1);
        res.put(Incoming, list2);
        res.put(Returned, list3);
        return res;
    }

    private boolean isStrInList(String str, LinkedList<String> list) {
        for (String tmp : list) {
            if (tmp.equals(str))
                return true;
        }
        return false;
    }

    private Section findInListByNumber(LinkedList<Section> secs, int number) {
        for (Section sec : secs) {
            if (sec.getNumber() == number)
                return sec;
        }
        throw new RuntimeException("Section Not Found");
    }


    public LinkedList<SupplyRejectionRequest> loadSupplyRejectionRequests(LinkedList<Supply> supplies) throws SQLException {
        LinkedList<SupplyRejectionRequest> res = new LinkedList<SupplyRejectionRequest>();
        ResultSet rs = null;
        try {
            rs = SqliteDbInterace.performSelectQuery(dbName,
                    "select * from SupplyRejectionRequest ");
            while (rs.next()) {
                res.add(
                        new SupplyRejectionRequest(
                                rs.getInt("supplyId"),
                                rs.getInt("creatorId"),
                                rs.getInt("processorId"),
                                rs.getInt("isProcessed") == 1,
                                rs.getInt("isArchived") == 1
                        )
                );
            }
            return res;
        } catch (SQLException e) {
            throw e;
        }

    }
}
