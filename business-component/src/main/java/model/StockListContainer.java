package model;


import java.util.LinkedList;

public class StockListContainer implements StockListInterface{
    private LinkedList<Stock> stocks = new LinkedList<>();

    public StockListContainer(){}

    public StockListContainer(Stock stock){
        stocks.add(stock);
    }

    public StockListContainer(LinkedList<Stock> stocks_){
        stocks.addAll(stocks_);
    }

    public boolean add (Stock stock){
        for (Stock st : stocks){
            if (st.getNumber() == stock.getNumber())
                return false;
        }
        stocks.add(stock);
        return true;
    }

    public Stock get (int number){
        for (Stock st : stocks){
            if (st.getNumber() == number)
                return st;
        }
        throw new RuntimeException("NumberNotFound");
    }
}
