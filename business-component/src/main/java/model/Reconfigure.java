package model;

import java.io.IOException;
import java.util.LinkedList;

public class Reconfigure {
    private DataManager dataManager;
    private UserInterface ui;

    public Reconfigure(DataManager dataManager_, UserInterface ui) {
        dataManager = dataManager_;
        this.ui = ui;
    }

    public void replaceSection(Section oldSection, Section newSection, int userId) throws IOException {
        if (oldSection != null) {
            dataManager.stocks().get(1).getSections().remove(oldSection);
        }
        if (newSection != null)
            dataManager.stocks().get(1).getSections().add(newSection);
//            dataManager.supplies().processSectionRemoval(oldSection, userId);
    }

    public Section input_section() throws IOException {
        Section defaultsection = null;
        defaultsection = new Section(1, 100, dataManager.stocks().get(1), 10, 0.4);
        return ui.enterNewSectionParams(defaultsection);
    }

    public boolean reconfigure(String action, int userId) throws IOException {
        LinkedList<Section> availableSections = null;
        availableSections = dataManager.stocks().get(1).getSections();
        Section sectionToModify = null;
        Section newSection = null;
        switch (action) {
            case ("add_section"):
                newSection = input_section();
                if (!ui.sectionAddApproveDialog(newSection))
                    return false;
                break;
            case ("change_section"):
                sectionToModify = dataManager.stocks().get(1).getSectionByNumber(ui.sectionChoiceDialog(null, availableSections));
                newSection = ui.enterNewSectionParams(sectionToModify);
                if (!ui.sectionModifyApproveDialog(newSection, sectionToModify))
                    return false;
                break;
            case ("delete_section"):
                sectionToModify = dataManager.stocks().get(1).getSectionByNumber(ui.sectionChoiceDialog(null, availableSections));
                ui.sectionDeleteOptionDialog();
                if (!ui.sectionDeleteApproveDialog(sectionToModify))
                    return false;
                break;
            default:
                break;
        }
        replaceSection(sectionToModify, newSection, userId);
        return true;
    }
}
