package model;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

class StringToJsonObject {
    public static JsonElement convert (String jsonStr) {
        JsonParser parser = new JsonParser();
        JsonElement jsonTree = parser.parse(jsonStr);
        return jsonTree;
    }
}