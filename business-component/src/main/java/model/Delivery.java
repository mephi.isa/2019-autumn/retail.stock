package model;

import java.io.IOException;
import java.util.*;

public class Delivery {
    private DataManager dataManager;
    private UserInterface ui;
    private SupplyPlanningSystemInterface spi;

    public Delivery(DataManager dataManager, UserInterface ui, SupplyPlanningSystemInterface spi) {
        this.dataManager = dataManager;
        this.ui = ui;
        this.spi = spi;
    }


    private LinkedList<Supply> getTodaySupplies() throws IOException {
        return spi.getTodaySupplies();
    }

    private void commitSupplyAcceptance(boolean isAccepted, Supply supply) {
        if (!spi.sendSupplyAcceptState(isAccepted, supply)) {
            System.out.println("Supply acceptance message to retail.suppliers failed");
        }
    }

    private int tryToPlace(int userId, Consignment targetConsignment) throws IOException {
        return Placement.placeSingleConsignment(dataManager.stocks().get(1), ui, userId, targetConsignment);
    }

    // TODO: data processing from planning system
    // TODO: подробная инф о помставке и список id-назв.компании
    public HashMap<String, Boolean> delivery(
            int userId,
            boolean userIsStockManager) throws IOException {

        HashMap<String, Boolean>res = new HashMap<String, Boolean>();
        res.put("rejection_request_created", false);
        res.put("goods_accepted", false);
        res.put("all_goods_placed", false);

        LinkedList<Supply> todaySupplies = spi.getTodaySupplies();
        dataManager.supplies().addSupplies(todaySupplies);

        Supply tgtSupply = dataManager.supplies().getSupplyByNumber(
                ui.supplyChoiceDialog(todaySupplies));

        if (ui.areGoodsInSupplyOkDialog(tgtSupply)){
            for (Consignment cons : tgtSupply.getConsignments()) {
                int placed_goods = tryToPlace(userId, cons);
                if (placed_goods == -1) {
                    ui.notify("There are no sections with suitable parameters for this consignment");
                    return res;
                }
                if (placed_goods != cons.getAmount()) {
                    res.put("all_goods_placed", false);
                }
            }
        } else {
            if (userIsStockManager) {
                commitSupplyAcceptance(false, tgtSupply);
                ui.notify("Supply successfully rejected!");
            } else {
                dataManager.supplyRejectionRequests().createRequest(tgtSupply, userId);
                res.put("rejection_request_created", true);
                ui.notify("Rejection request created!");
            }
        }
        return res; // TODO: use Enum or smth else less complicated than HashMap as return object?
    }

    public boolean processSupplyFromRejectionRequests(int userId, int requestNumber) throws IOException {
        SupplyRejectionRequest req = null;
        req = dataManager.supplyRejectionRequests().getRequestBySupplyNumber(requestNumber);
        boolean all_goods_placed = true;
        if (!req.getIsProcessed()) return all_goods_placed;
        for (Consignment cons : dataManager.supplies().getSupplyByNumber(
                req.getSupplyNumber()).getConsignments()) {
            if (tryToPlace(userId, cons) < cons.getAmount()) {
                all_goods_placed = false;
            }
            req.archivate();
        }
        return all_goods_placed;
    }

    // Вынести взаимодействие с пользователем из функций бизнес-логики
    // В передаче использовать простые значения, а не бизнес-объекты

    public void supplyRejectionRequestsProcessing(boolean userIsStockManager, int requestNumber) throws IOException {
        if (!userIsStockManager) {
            System.out.print("Insufficient rights to use supplyRejectionRequestsProcessing\n");
            return;
        }
        SupplyRejectionRequest req = null;
        req = dataManager.supplyRejectionRequests().getRequestBySupplyNumber(requestNumber);
        Supply correspondingSupply = dataManager.supplies().getSupplyByNumber(req.getSupplyNumber());

        boolean approveRequest = ui.approveSupplyRejectionRequest(req, correspondingSupply);
        // Выбор запроса из списка
        if (approveRequest) {
            req.approve();
            correspondingSupply.decline("");
            commitSupplyAcceptance(false, correspondingSupply);
            req.archivate();
            ui.notify("Request approved => supply rejected");
        } else {
            req.decline();
            correspondingSupply.approve();
            commitSupplyAcceptance(true, correspondingSupply);
            ui.notify("Request rejected => supply approved");
        }

    }
}
