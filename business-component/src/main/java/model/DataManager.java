package model;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

public class    DataManager {
    private StockListInterface stocks;
    private SupplyListInterface supplies_;
    private SupplyRejectionRequestsInterface supplyRejectionRequests_;

    //    private ConsignmentWriteOffRequestsContainer consignmentWriteOffRequests;
    public DataManager(StockListInterface slc, SupplyListInterface supplies, SupplyRejectionRequestsContainer supplyRejectionRequests) {
        stocks = slc;
        supplies_ = supplies;
        supplyRejectionRequests_ = supplyRejectionRequests;
    }

    public StockListInterface stocks() {
        return stocks;
    }

    public SupplyListInterface supplies() {
        return supplies_;
    }

    public SupplyRejectionRequestsInterface supplyRejectionRequests() {
        return supplyRejectionRequests_;
    }

    public void saveStock(String dbFileName, int stockNum) throws IOException, SQLException {
        DbSaver ds = new DbSaver(dbFileName);
        ds.saveStock(stocks.get(stockNum), supplies_.getSupplies(), supplyRejectionRequests_.getRequests());
    }

    public void loadStock(String dbFileName, int stockNum) throws IOException, SQLException {
        DbLoader dl = new DbLoader(dbFileName);
        Stock newStock = dl.loadStock(stockNum);
        LinkedList<Supply> supplies = dl.loadSupplies(newStock);
        LinkedList<SupplyRejectionRequest> reqs = dl.loadSupplyRejectionRequests(supplies);

        stocks.add(newStock);

    }


}
