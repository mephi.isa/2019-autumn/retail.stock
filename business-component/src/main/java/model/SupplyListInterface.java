package model;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public interface SupplyListInterface {
    public boolean addSupply(Supply newSupply);
    public boolean addSupplies(List<Supply> newSupply);
    public Supply getSupplyByNumber(int num);
    public LinkedList<Supply> getSupplies();
    public LinkedList<Consignment> getExpiredProductsByDate(Date tgtDate);
    public LinkedList<Consignment> getConsignmentsWithWriteOffRequests();
    public LinkedList<Consignment> getConsignmentsWithUnplacedGoods();
    public HashMap<Consignment, HashMap<Section, Integer>> getConsignmentsListForTransfer();
    public LinkedList<Consignment> getConsignmentsOnSale();
}
