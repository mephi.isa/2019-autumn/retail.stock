package model;

import java.util.*;

public interface UserInterface {
    void notify (String message);

    int chooseAction(List<String> opts);

    int supplyChoiceDialog(LinkedList<Supply> supplies);

    boolean isSupplyPresentDialog(Supply supply);

    boolean areGoodsInSupplyOkDialog(Supply supply);

    boolean approveSupplyDialog(Supply supply);

    int chooseSupplyRejectionRequest(LinkedList<SupplyRejectionRequest> requests);

    boolean approveSupplyRejectionRequest(SupplyRejectionRequest req, Supply supply);

    String getRejectionReason(Supply supply);
    // /support Delivery

    // support Placement
    Consignment consignmentChoiceDialog(LinkedList<Consignment> consignments);

    public int sectionChoiceDialog(Consignment cons, LinkedList<Section> sections);

    void notEnoughSpaceWarning();

    int howManyGoodsPlacedDialog(boolean amountWarning, Consignment consignment, Section section);
    // /support Placement

    // support WriteOff
    String writeOffReasonDialog(Consignment consignments);

    boolean writeOffApproveDialog(Consignment cons);
    // /support WriteOff

    // support Reconfigure
    String reconfigurationOptionDialog();

    boolean sectionAddApproveDialog(Section section);

    boolean sectionModifyApproveDialog(Section oldSection, Section newSection);

    boolean sectionDeleteApproveDialog(Section section);

    String sectionDeleteOptionDialog();

    Section enterNewSectionParams(Section oldSection);

    String consignmentToTransferChoiceDialog(HashMap<Consignment, HashMap<Section, Integer>> options);

    boolean continueTransferDialog();

    int whichAmountToTransfer(Consignment consignment);

    void consignmentSearchInfoDisplay(Consignment cons);

    boolean isConsignmentSearchSuccessful();

    Consignment chooseConsignmentFromReturnList(LinkedList<Consignment> options);

    String getReturnReason(Consignment cons);

    boolean isReturnedConsignmentOk(Consignment cons);

    String getLogin(String message);

}
