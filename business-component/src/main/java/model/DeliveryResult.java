package model;

public enum DeliveryResult {
    GoodsNotAccepted, GoodsAcceptedAndNotPlaced,
    GoodsAcceptedAndPlaced, RejectionRequestCreated
}
