package model;

import org.apache.commons.lang3.Range;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

public interface SupplyPlanningSystemInterface {
    LinkedList<Supply> getTodaySupplies() throws IOException;

    boolean sendSupplyAcceptState(boolean isAccepted, Supply supply);

    boolean sendConsignmentRejectedMessage(Consignment cons);
}
