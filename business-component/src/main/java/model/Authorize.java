package model;

public class Authorize {
    UserInterface ui;
    PersonelSystem ps = new PersonelSystemEmulator();

    public Authorize(UserInterface u) {
        ui = u;
    }

    public boolean authorize(boolean isUserShouldBeManager) {
        String login = ui.getLogin("");
        return (isUserShouldBeManager == ps.userIsManager(ps.getIdByLogin(login)));
    }
}
