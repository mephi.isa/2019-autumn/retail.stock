package model;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

public class ReturnFromSale {
    DataManager dataManager;
    UserInterface ui;

    public ReturnFromSale(DataManager d, UserInterface u){
        dataManager = d; ui = u;
    }

    public void returnFromSale(long userId){
        LinkedList<Consignment> returnList = dataManager.supplies().getConsignmentsOnSale();
        while (returnList.size() != 0) {
            Consignment targetCons = ui.chooseConsignmentFromReturnList(returnList);
            String reason = ui.getReturnReason(targetCons);
            targetCons.backFromSale(targetCons.getOnSaleAmount(), userId);
            if (ui.isReturnedConsignmentOk(targetCons)){
                // вообще, может ли быть отправлена в торговый зал только часть товаров????
                // то же самое, мы пытаемся разместить весть товар, несмотря на то, что могла быть возвращена только часть
                try {
                    Placement.placeSingleConsignment(dataManager.stocks().get(1), ui, userId, targetCons);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                // списывается весь товар, считается, что reason списания относится ко всему Consignment
                targetCons.writeOffByReason(ui.writeOffReasonDialog(targetCons), userId);
            }
            returnList.remove(targetCons);
        }
    }
}
