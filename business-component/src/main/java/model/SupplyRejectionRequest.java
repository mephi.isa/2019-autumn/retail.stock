package model;

public class SupplyRejectionRequest {
    private int supplyNumber;
    private long creatorId;
    private long processorId;
    private boolean isProcessed;
    private boolean isArchived;

    public SupplyRejectionRequest(int supplyNumber, long creatorId, long processorId, boolean isProcessed, boolean isArchived){
        this.supplyNumber = supplyNumber; this.isProcessed = false; this.creatorId = creatorId;
        this.processorId = processorId; this.isProcessed = isProcessed; this.isArchived = isArchived;
    }

    public void approve(){
        if (isArchived) return;
        this.isProcessed = true;
    }

    public void decline(){
        if (isArchived) return;
        this.isProcessed = true;
    }

    public void archivate(){
        isArchived = true;
    }

    public boolean getIsProcessed(){
        return isProcessed;
    }

    public boolean getIsArchived(){
        return isArchived;
    }

    public int getSupplyNumber() {
        return supplyNumber;
    }

    public long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(long creatorId) {
        this.creatorId = creatorId;
    }

    public long getProcessorId() {
        return processorId;
    }

    public void setProcessorId(long processorId) {
        this.processorId = processorId;
    }
}