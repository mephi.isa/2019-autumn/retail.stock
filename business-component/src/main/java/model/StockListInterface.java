package model;

import java.io.IOException;

public interface StockListInterface {
    public boolean add (Stock stock);
    public Stock get (int number) throws IOException;
}
