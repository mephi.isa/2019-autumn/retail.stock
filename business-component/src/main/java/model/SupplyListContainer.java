package model;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class SupplyListContainer implements SupplyListInterface { // TODO: Interface
    private LinkedList<Supply> supplyList;

    public SupplyListContainer(){
        supplyList = new LinkedList<Supply>();
    }

    @Override
    public LinkedList<Supply> getSupplies() {
        return supplyList;
    }

    public boolean addSupply(Supply newSupply){
        if (newSupply == null) return false;
        supplyList.add(newSupply);
        return true;
    }

    @Override
    public boolean addSupplies(List<Supply> newSupply) {
        for (Supply sup : newSupply){
            addSupply(sup);
        }
        return true;
    }

    public Supply getSupplyByNumber(int num){
        for(Supply supply : supplyList){
            if (supply.getNumber() == num)
                return supply;
        }
        throw new RuntimeException("num not found");
    }

    public LinkedList<Consignment> getExpiredProductsByDate(Date tgtDate){
        LinkedList<Consignment> res = new LinkedList<Consignment>();
        for(Supply supply : supplyList)
            for (Consignment cons : supply.getConsignments())
                if (cons.isExpired(tgtDate))
                    res.add(cons);
        return res;
    }

    public LinkedList<Consignment> getConsignmentsWithWriteOffRequests(){
        LinkedList<Consignment> res = new LinkedList<Consignment>();
        for(Supply supply : supplyList)
            for (Consignment cons : supply.getConsignments())
                if (cons.getWrittenOff() != null)
                    res.add(cons);
        return res;
    }

    public LinkedList<Consignment> getConsignmentsWithUnplacedGoods(){
        LinkedList<Consignment> res = new LinkedList<Consignment>();
        for(Supply supply : supplyList)
            for (Consignment cons : supply.getConsignments())
                if (cons.getUnplacedAmount() != 0)
                    res.add(cons);
        return res;
    }

    public HashMap<Consignment, HashMap<Section, Integer>> getConsignmentsListForTransfer(){
        HashMap<Consignment, HashMap<Section, Integer>> res = new HashMap<Consignment, HashMap<Section, Integer>>();
        for (Supply supply : supplyList){
            for (Consignment consignment : supply.getConsignments()){
                res.put(consignment, consignment.getAmountBySection());
            }
        }
        return res;
    }

    public LinkedList<Consignment> getConsignmentsOnSale(){
        LinkedList<Consignment> res = new LinkedList<Consignment>();
        for (Supply supply : supplyList){
            for (Consignment consignment : supply.getConsignments()){
                if (consignment.getOnSaleAmount() != 0)
                    res.add(consignment);
            }
        }
        return res;
    }
}
