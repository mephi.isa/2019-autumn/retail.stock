package model;

import java.io.IOException;
import java.util.LinkedList;

public class Placement {
    private DataManager dataManager;
    private UserInterface ui;

    public Placement(DataManager dataManager, UserInterface ui) {
        this.dataManager = dataManager;
        this.ui = ui;
    }

    public static boolean conditionsMatched(Section sec, Consignment cons) {
        return (cons.getHumidityRange().contains(sec.getHumidity())
                && cons.getTemperatureRange().contains(sec.getTemperature()));
    }

    public static LinkedList<Section> getSectionsForConsignment(Stock stock, Consignment cons) {
        LinkedList<Section> allSections = (LinkedList<Section>) stock.getSections(),
                res = new LinkedList<Section>();
        for (Section section : allSections) {
            if (conditionsMatched(section, cons))
                res.add(section);
        }
        return res;
    }

    public static int placeSingleConsignment(Stock stock, UserInterface ui, long userId, Consignment chosenConsignment) {
        LinkedList<Section> sectionsWithRightConditions = getSectionsForConsignment(stock, chosenConsignment);
        if (sectionsWithRightConditions.size() == 0) return -1;
        Section chosenSection = stock.getSectionByNumber(ui.sectionChoiceDialog(chosenConsignment,sectionsWithRightConditions));
        boolean amountWarning = false;
        if (chosenConsignment.getAmount() > (chosenSection.getMaxAmount() - chosenConsignment.getAmount())) {
            amountWarning = true;
        }
        int placedNum = ui.howManyGoodsPlacedDialog(amountWarning, chosenConsignment, chosenSection);
        if (placedNum > 0) {
            chosenConsignment.placing(chosenSection, placedNum, userId);
        }
        return placedNum;
    }

    /*Предполагается, что за один запуск функциии происходит размещение всех неразмещенных товаров из группы (Consignment).
    Это упрощение сделано намеренно*/
    public int place(int userId, LinkedList<Consignment> unplacedGoods) throws IOException {
        // TODO: бизнес-процесс должен сам запрашивать список неразмещенных партий
        Consignment chosenConsignment = ui.consignmentChoiceDialog(unplacedGoods);
        int res = 0;
        res = placeSingleConsignment(this.dataManager.stocks().get(1), this.ui, userId, chosenConsignment);
        return (res);
    }
}
