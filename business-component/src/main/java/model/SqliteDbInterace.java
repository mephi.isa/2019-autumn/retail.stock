package model;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;

class SqliteDbInterace {
    private static final String createStockTablesQueryFilePath = new String("/home/kds/univer/magister/information_systems/retail.stock/project_root/business-component/createTables.sql");

    static private String fileToStr(String path) throws IOException {
        InputStream is = new FileInputStream(path);
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));

        String line = buf.readLine();
        StringBuilder sb = new StringBuilder();

        while (line != null) {
            sb.append(line).append("\n");
            line = buf.readLine();
        }

        return sb.toString();
    }

    static void createStockTables(String databaseFilename) throws SQLException, IOException {
        performUpdateQuery(databaseFilename, fileToStr(createStockTablesQueryFilePath));

    }

    static void performUpdateQuery(String databaseFilename, String query) throws SQLException {
        Connection conn = DriverManager.getConnection(
                "jdbc:sqlite:" + databaseFilename);

        Statement stmt = conn.createStatement();
        stmt.executeUpdate(query);
    }

    static ResultSet performSelectQuery(String databaseFilename, String query) throws SQLException {
        Connection conn = DriverManager.getConnection(
                "jdbc:sqlite:" + databaseFilename);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        return rs;
    }


    static int getLastInsertRowId(String databaseFilename) throws SQLException {
        ResultSet rs = performSelectQuery(databaseFilename, "select last_insert_rowid()");
        return rs.getInt("last_insert_rowid()");
    }
}