package model;
// TODO: Recommendation move tedsting class to tests
import java.awt.*;
import java.util.*;
import java.util.List;

public class UserInterfaceEmulator implements UserInterface {
    private HashMap<String, String> answerMap = new HashMap<String, String>();

    public UserInterfaceEmulator() {
        resetAnswerMap();
    }



    public void resetAnswerMap() {
        answerMap.put("supplyChoiceDialog", "0");
        answerMap.put("isSupplyPresentDialog", "true");
        answerMap.put("areGoodsInSupplyOkDialog", "true");
        answerMap.put("approveSupplyDialog", "true");
        answerMap.put("chooseSupplyRejectionRequest", "0");
        answerMap.put("approveSupplyRejectionRequest", "true");
        answerMap.put("getRejectionReason", "Some test reason");

        answerMap.put("consignmentChoiceDialog", "0");
        answerMap.put("sectionChoiceDialog", "0");
        answerMap.put("howManyGoodsPlacedDialog", "1");

        answerMap.put("sectionAddApproveDialog", "true");
        answerMap.put("sectionModifyApproveDialog", "true");
        answerMap.put("sectionDeleteApproveDialog", "true");
        answerMap.put("sectionDeleteOptionDialog", "0");

        answerMap.put("chooseConsignmentFromReturnList", "0");
        answerMap.put("isReturnedConsignmentOk", "true");

        answerMap.put("writeOffReasonDialog", "Consignment expired");
        answerMap.put("writeOffApproveDialog", "true");

        answerMap.put("continueTransferDialog", "true");
        answerMap.put("whichAmountToTransfer", "1");
        answerMap.put("consignmentToTransferChoiceDialog", "art1");
    }

    public HashMap<String, String> getAnswerMap() {
        return answerMap;
    }

    public String getAnswer(String funName) {
        return answerMap.get(funName);
    }

    public boolean setAnswer(String funName, String newAnswer) {
        if (answerMap.containsKey(funName)) {
            answerMap.put(funName, newAnswer);
            return true;
        }
        return false;
    }

    // templates
    private Boolean booleanChoice(String funName) {
        boolean res = Boolean.parseBoolean(answerMap.get(funName));
        System.out.printf("Automatic dialog: fun %s, returned %s\n", funName, res);
        return res;
    }

    private String stringChoice(String funName) {
        String res = answerMap.get(funName);
        System.out.printf("Automatic dialog: fun %s, returned %s\n", funName, res);
        return res;
    }

    private int integerChoice(String funName) {
        int res = Integer.parseInt(answerMap.get(funName));
        System.out.printf("Automatic dialog: fun %s, returned %s\n", funName, res);
        return res;
    }

//    private <T1, T2> T1 oneFromHashMapChoice(String funName, HashMap<T1, T2> options){
//        int res = Integer.parseInt(answerMap.get(funName));
//        for (Map.Entry<T1, T2> entry : options.entrySet()) {
//            System.out.printf("Entry: (%s, %s)\n", entry.getKey(), entry.getValue());
//        }
//        int index = Integer.parseInt(answerMap.get(funName));
//
//        System.out.printf("Automatic dialog: fun %s, returned %s\n", funName, list.get(index));
//        return options.;
//    }

    private <T> T oneFromListChoice(String funName, LinkedList<T> list) {
        System.out.printf("Automatic dialog: fun %s possible choices:\n", funName);
        for (int i = 0; i < list.size(); ++i) {
            System.out.printf("Entry #%d: %s\n", i, list.get(i));
        }
        int index = Integer.parseInt(answerMap.get(funName));

        System.out.printf("Automatic dialog: fun %s, returned %s\n", funName, list.get(index));
        return list.get(index);
    }

    public <T> T setChoice(String funName, Set<T> options) {
        System.out.printf("Automatic dialog: fun %s possible choices:\n", funName);
        for (T item : options) {
            System.out.printf("Entry %s\n", item);
        }
        int index = Integer.parseInt(answerMap.get(funName));
        int i = 0;
        for (T item : options) {
            if (i == index)
                return item;
            i = i + 1;
        }
        return null;
    }

    @Override
    public void notify(String message) {
        System.out.print(message);
    }

    @Override
    public int chooseAction(List<String> opts) {
        return 0;
    }

    // support Delivery
    public int supplyChoiceDialog(LinkedList<Supply> supplies) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        Supply supply = oneFromListChoice(funName, supplies);
        if (isSupplyPresentDialog(supply)){
            return supply.getNumber();
        } else {
            return -1;
        }
    }

    public boolean isSupplyPresentDialog(Supply supply) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return booleanChoice(funName);
    }

    public boolean areGoodsInSupplyOkDialog(Supply supply) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return booleanChoice(funName);
    }

    public boolean approveSupplyDialog(Supply supply) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return booleanChoice(funName);
    }

    public int chooseSupplyRejectionRequest(LinkedList<SupplyRejectionRequest> requests){
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return oneFromListChoice(funName, requests).getSupplyNumber();
    }

    public boolean approveSupplyRejectionRequest(SupplyRejectionRequest req, Supply supply) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return booleanChoice(funName);
    }

    public String getRejectionReason(Supply supply) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return stringChoice(funName);
    }
    // /support Delivery

    // support Placement
    public Consignment consignmentChoiceDialog(LinkedList<Consignment> consignments) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return oneFromListChoice(funName, consignments);
    }

    public int sectionChoiceDialog(Consignment cons, LinkedList<Section> sections) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return oneFromListChoice(funName, sections).getNumber();
    }

    public void notEnoughSpaceWarning(){}

    public int howManyGoodsPlacedDialog(boolean warnMessage, Consignment consignment, Section section) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return integerChoice(funName);
    }
    // /support Placement

    // support WriteOff
    public String writeOffReasonDialog(Consignment consignments) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return stringChoice(funName);
    }

    public boolean writeOffApproveDialog(Consignment cons) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return booleanChoice(funName);
    }
    // /support WriteOff

    // support Reconfigure
    public String reconfigurationOptionDialog() {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        LinkedList<String> options = new LinkedList<String>();
        options.add("add_section");
        options.add("change_section");
        options.add("delete_section");
        return oneFromListChoice(funName, options);
    }


    public boolean sectionAddApproveDialog(Section section) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return booleanChoice(funName);
    }

    public boolean sectionModifyApproveDialog(Section oldSection, Section newSection) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return booleanChoice(funName);
    }

    public boolean sectionDeleteApproveDialog(Section section) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return booleanChoice(funName);
    }

    public String sectionDeleteOptionDialog() {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        LinkedList<String> options = new LinkedList<String>();
        options.add("option1");
        options.add("option2");
        options.add("option3");
        return oneFromListChoice(funName, options);
    }

    public Section enterNewSectionParams(Section oldSection) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return new Section(1, 100, oldSection.getStock(), 10, 0.4);
    }

    public String consignmentToTransferChoiceDialog(HashMap<Consignment, HashMap<Section, Integer>> options) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        String art = answerMap.get(funName);
        for (Map.Entry<Consignment, HashMap<Section, Integer>> entry : options.entrySet()) {
            System.out.printf("Entry: (%s, %s)\n", entry.getKey().getArticle(), entry.getValue());
        }
        System.out.printf("Automatic dialog: fun %s, returned %s\n", funName, art);
        for (Map.Entry<Consignment, HashMap<Section, Integer>> entry : options.entrySet()) {
            if (entry.getKey().getArticle().equals(art))
                return art;
        }
        return null;
    }

    public boolean continueTransferDialog() {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return booleanChoice(funName);
    }

    public int whichAmountToTransfer(Consignment consignment) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return integerChoice(funName);
    }

    public void consignmentSearchInfoDisplay(Consignment cons) {
        System.out.printf("Search for Consignment %s in %s", cons.getArticle(), cons.getAmountBySection());
    }

    public boolean isConsignmentSearchSuccessful() {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return booleanChoice(funName);
    }

    public Consignment chooseConsignmentFromReturnList(LinkedList<Consignment> options) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return oneFromListChoice(funName, options);
    }

    public String getReturnReason(Consignment cons) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return stringChoice(funName);
    }

    public boolean isReturnedConsignmentOk(Consignment cons) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return booleanChoice(funName);
    }

    public String getLogin(String message) {
        String funName = new Throwable().getStackTrace()[0].getMethodName();
        return stringChoice(funName);
    }
}
