package model;

import javax.swing.*;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class GUI implements UserInterface {

    public GUI() {

    }

    @Override
    public void notify(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    @Override
    public int chooseAction(List<String> opts) {
        StringBuilder res = new StringBuilder();
        MainDialog mainDialog = new MainDialog(res, opts);
        mainDialog.setSize(200, 200);
        mainDialog.setVisible(true);
        if (res.toString().equals("\0")) return -1;
//        JOptionPane.showMessageDialog(null, "Your Login: \"" + res + "\"");
        return opts.indexOf(res.toString());
    }

    @Override
    public int supplyChoiceDialog(LinkedList<Supply> supplies) {
        StringBuilder res = new StringBuilder();

        String[] cols = new String[]{
                "number", "provider", "consignments num", "date", "status"
        };

        LinkedList<Object[]> data = new LinkedList<>();
        for (Supply sup : supplies) {
            String status = sup.getStatus().isDeclined() ?
                    "declined" : sup.getStatus().isApproved() ?
                    "approved" : "undefined";
            data.add(new Object[]{
                    sup.getNumber(), sup.getProvider(),
                    sup.getConsignments().size(),
                    DateConverter.dateToString(sup.getDate()),
                    status
            });
        }

        SupplyChoiceDialog dialog = new SupplyChoiceDialog(cols, data, res);
        dialog.setSize(400, 400);
        dialog.setVisible(true);
        if (res.toString().equals("\0")) return -1;
        return Integer.parseInt(res.toString());
    }

    @Override
    public boolean isSupplyPresentDialog(Supply supply) {
        return false;
    }

    @Override
    public boolean areGoodsInSupplyOkDialog(Supply supply) {
        StringBuilder res = new StringBuilder();

        String[] cols = new String[]{
                "article", "expiration date", "amount", "required temperature range", "required humidity range"
        };

        LinkedList<Object[]> data = new LinkedList<>();
        for (Consignment cons : supply.getConsignments()) {
            data.add(new Object[]{
                    cons.getArticle(),
                    DateConverter.dateToString(cons.getExpDate()),
                    cons.getAmount(),
                    cons.getTemperatureRange().toString(),
                    cons.getHumidityRange().toString()
            });
        }

        AreGoodsInSupplyOkDialog dialog =
                new AreGoodsInSupplyOkDialog (cols, data, res);
        dialog.setSize(400, 400);
        dialog.setVisible(true);
        return Boolean.parseBoolean(res.toString());
    }

    @Override
    public boolean approveSupplyDialog(Supply supply) {
        return false;
    }

    @Override
    public int chooseSupplyRejectionRequest(LinkedList<SupplyRejectionRequest> requests) {
        StringBuilder res = new StringBuilder();

        String[] cols = new String[]{
                "supply number", "creator id", "status"
        };

        LinkedList<Object[]> data = new LinkedList<>();
        for (SupplyRejectionRequest srr : requests) {
            String status = srr.getIsProcessed() ?
                    "processed" : srr.getIsArchived() ?
                    "archived" : "unprocessed";
            data.add(new Object[]{
                    srr.getSupplyNumber(),
                    srr.getCreatorId(),
                    status,
            });
        }

        SupplyChoiceDialog dialog = new SupplyChoiceDialog(cols, data, res);
        dialog.setSize(400, 400);
        dialog.setVisible(true);
        if (res.toString().equals("\0")) return -1;
        return Integer.parseInt(res.toString());
    }

    @Override
    public boolean approveSupplyRejectionRequest(SupplyRejectionRequest req, Supply supply) {
        StringBuilder res = new StringBuilder();

        String info = String.format("Supply information:\n" +
                "#%d,\n" +
                "provider: %s\n" +
                "date: %s, \n", req.getSupplyNumber(), supply.getProvider(), DateConverter.dateToString(supply.getDate()));

        ApproveSupplyRejectionRequest dialog = new ApproveSupplyRejectionRequest(res, info);
        dialog.setSize(400, 400);
        dialog.setVisible(true);
        return Boolean.parseBoolean(res.toString());
    }

    @Override
    public String getRejectionReason(Supply supply) {
        return null;
    }

    @Override
    public Consignment consignmentChoiceDialog(LinkedList<Consignment> consignments) {
        return null;
    }

    @Override
    public int sectionChoiceDialog(Consignment cons, LinkedList<Section> sections) {
        StringBuilder res = new StringBuilder();

        String[] cols = new String[]{
                "section number", "max amount", "temperature", "humidity"
        };

        LinkedList<Object[]> data = new LinkedList<>();
        for (Section sec : sections) {
            data.add(new Object[]{
                    sec.getNumber(),
                    sec.getMaxAmount(),
                    sec.getTemperature(),
                    sec.getHumidity()
            });
        }

        SectionChoiceDialog dialog = new SectionChoiceDialog(cols, data, res,
                cons == null ? "" : cons.getArticle() + ", amount = " + cons.getAmount());
        dialog.setSize(400, 400);
        dialog.setVisible(true);
        if (res.toString().equals("\0")) return -1;
        return Integer.parseInt(res.toString());
    }

    @Override
    public void notEnoughSpaceWarning() {

    }

    @Override
    public int howManyGoodsPlacedDialog(boolean amountWarning, Consignment consignment, Section section) {
        StringBuilder result = new StringBuilder();
        String consInfo = consignment.getArticle() + ", amount = " + consignment.getAmount();
        String sectionInfo = "# " + section.getNumber();
        HowManyGoodsPlacedDialog dialog =
                new HowManyGoodsPlacedDialog(result, consInfo, sectionInfo);
        dialog.setSize(400, 400);
        dialog.setVisible(true);
        if (result.toString().equals("\0")) return -1;
        return Integer.parseInt(result.toString());
    }

    @Override
    public String writeOffReasonDialog(Consignment consignments) {
        return null;
    }

    @Override
    public boolean writeOffApproveDialog(Consignment cons) {
        return false;
    }

    @Override
    public String reconfigurationOptionDialog() {
        return null;
    }

    @Override
    public boolean sectionAddApproveDialog(Section section) {
        return false;
    }

    @Override
    public boolean sectionModifyApproveDialog(Section oldSection, Section newSection) {
        return false;
    }

    @Override
    public boolean sectionDeleteApproveDialog(Section section) {
        return false;
    }

    @Override
    public String sectionDeleteOptionDialog() {
        return null;
    }

    @Override
    public Section enterNewSectionParams(Section oldSection) {
        return null;
    }

    @Override
    public String consignmentToTransferChoiceDialog(HashMap<Consignment, HashMap<Section, Integer>> options) {
        return null;
    }

    @Override
    public boolean continueTransferDialog() {
        return false;
    }

    @Override
    public int whichAmountToTransfer(Consignment consignment) {
        return 0;
    }

    @Override
    public void consignmentSearchInfoDisplay(Consignment cons) {

    }

    @Override
    public boolean isConsignmentSearchSuccessful() {
        return false;
    }

    @Override
    public Consignment chooseConsignmentFromReturnList(LinkedList<Consignment> options) {
        return null;
    }

    @Override
    public String getReturnReason(Consignment cons) {
        return null;
    }

    @Override
    public boolean isReturnedConsignmentOk(Consignment cons) {
        return false;
    }

    @Override
    public String getLogin(String message) {
        StringBuilder res = new StringBuilder();
        AuthorizeDialog dialog = new AuthorizeDialog(res, message);
        dialog.setSize(200, 200);
        dialog.setVisible(true);
        //JOptionPane.showMessageDialog(null, "Your Login: \"" + res.toString() + "\"");
        return res.toString();
    }
}
