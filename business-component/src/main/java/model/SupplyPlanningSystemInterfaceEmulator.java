package model;

import org.apache.commons.lang3.Range;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

public class SupplyPlanningSystemInterfaceEmulator implements SupplyPlanningSystemInterface{
    public LinkedList<Supply> getTodaySupplies() {
        LinkedList<Supply> res = new LinkedList<Supply>();
        Supply tmp = new Supply(1, "provider1", new Date());
        Supply tmp2 = new Supply(2, "provider2", new Date());
        Supply tmp3 = new Supply(3, "provider3", new Date());
        Date date = new Date(2020, Calendar.JANUARY, 1);
        Consignment cons1 = new Consignment(
                "art1",
                date,
                10,
                tmp,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        Consignment cons2 = new Consignment(
                "art2",
                date,
                20,
                tmp,
                Range.between(-20, 20),
                Range.between(0.1, 0.9)
        );
        Consignment cons3 = new Consignment(
                "art3",
                date,
                30,
                tmp,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        Consignment cons4 = new Consignment(
                "art4",
                date,
                40,
                tmp,
                Range.between(-20, 20),
                Range.between(0.1, 1.0)
        );
        Consignment cons5 = new Consignment(
                "art5",
                date,
                50,
                tmp,
                Range.between(-10, 10),
                Range.between(0.0, 1.0)
        );
        Consignment cons6 = new Consignment(
                "art6",
                date,
                60,
                tmp,
                Range.between(-20, 20),
                Range.between(0.1, 1.0)
        );
        tmp.addConsignment(cons1); tmp.addConsignment(cons2);
        tmp2.addConsignment(cons3); tmp2.addConsignment(cons4);
        tmp3.addConsignment(cons5); tmp3.addConsignment(cons6);

        res.add(tmp); res.add(tmp2); res.add(tmp3);
        return res;
    }

    public boolean sendSupplyAcceptState (boolean isAccepted, Supply supply){
        System.out.printf("Supply accept state sent. Supply: %s, assepted?: %s\n", supply, isAccepted);
        boolean isMessage_recieved = true;

        return isMessage_recieved;
    }

    public boolean sendConsignmentRejectedMessage (Consignment cons){
        System.out.printf("Supply accept state sent. Consignment: %s\n", cons);
        boolean isMessage_recieved = true;

        return isMessage_recieved;
    }
}
