package model;

import org.apache.commons.lang3.Range;

import java.io.IOException;
import java.util.*;
//import javafx.application.Application;
//import javafx.stage.Stage;

//class Main extends Application {
class Main {
    static private DataManager dataManager = new DataManager(
            new StockListContainer(
                    new Stock(1, "Lenina st., 1")
            ),
            new SupplyListContainer(),
            new SupplyRejectionRequestsContainer());
    static private UserInterface ui = new GUI();
    static private SupplyPlanningSystemInterface spi = new SupplyPlanningSystem("192.168.56.101");
    static private PersonelSystem ps = new PersonelSystemEmulator();

    static private Delivery delivery = new Delivery(dataManager, ui, spi);
    static private Placement placement = new Placement(dataManager, ui);

    public static void main(String[] args) throws IOException {
        int userId = 1;
        boolean userIsManager = false;

        LinkedList<String> opts = new LinkedList<String>(Arrays.asList(
                "authorize",
                "delivery / accept goods",
                "delivery / process supply rejection requests",
                "delivery / process supplies from rejection requests",
                "place",
                "reconfigure",
                "return from shop",
                "transfer",
                "write off",
                "save data",
                "load data",
                "exit"));

        dataManager.stocks().get(1).getSections().add(
                new Section(1, 20, dataManager.stocks().get(1), 0, 0.5)
        );
        while (true) {
            int action = ui.chooseAction(opts);
            switch (action) {
                case (0):
                    userId = ps.getIdByLogin(ui.getLogin("Enter your login"));
                    userIsManager = ps.userIsManager(userId);
                    String msg = userIsManager ? "You are Manager!" : "You are NOT manager";
                    ui.notify(msg);
                    break;
                case 1:
                    HashMap<String, Boolean> res
                            = delivery.delivery(userId, userIsManager);
                    break;
                case 2:
                    if (!userIsManager)
                        break;
                    int tgtSupplyNumber = ui.chooseSupplyRejectionRequest(
                            dataManager.supplyRejectionRequests().getRequests()
                    );
                    delivery.supplyRejectionRequestsProcessing(
                            userIsManager,
                            tgtSupplyNumber);
                    break;
                case 3:
                    tgtSupplyNumber = ui.chooseSupplyRejectionRequest(
                            dataManager.supplyRejectionRequests().getRequests()
                    );
                    delivery.processSupplyFromRejectionRequests(
                            userId, tgtSupplyNumber);
                    break;
                case 11:
                    return;
            }
        }
    }
}



