package model;

import java.io.IOException;
import java.util.LinkedList;

public interface SupplyRejectionRequestsInterface {
    public Boolean createRequest(Supply supply, long userId);
    public LinkedList<SupplyRejectionRequest> getRequests() throws IOException;
    public LinkedList<Integer> getSuppliesNumbersFromRequests(Boolean isArchived, Boolean isProcessed) throws IOException;
//    public LinkedList<Supply> getSuppliesFromRequests(Boolean isArchived, Boolean isProcessed, Boolean isApproved);
    public SupplyRejectionRequest getRequestBySupplyNumber(int number) throws IOException;
    public LinkedList<Integer> getPlaceableSuppliesNumbersFromRequests() throws IOException;
}
