create table if not exists Consignment
(
    article          text
        primary key,
    expDate          text    not null,
    amount           integer not null,
    returnedAmount   integer not null,
    tempRangeMax     real    not null,
    tempRangeMin     real    not null,
    humidityRangeMax real    not null,
    humidityRangeMin real    not null,
    wo_userId        integer ,
    wo_reason        text    ,
    wo_approved      integer ,
    wo_date          text
);

create table if not exists Section
(
    number          integer
        primary key,
    maxAmount   integer not null,
    temperature integer    not null,
    humidity    real    not null
);

create table if not exists Operation
(
    id         integer
        primary key autoincrement ,
    type       text    not null,
    sectionId  integer
        references Section,
    amount     integer not null,
    invokerId  integer not null,
    recieverId integer not null,
    date       text    not null
);

create table if not exists OperationsSet
(
    consignmentArticle text
        references Consignment,
    operationId        integer
        references Operation,
    primary key (consignmentArticle, operationId)
);

create table if not exists Stock
(
    number  integer
        primary key,
    address text not null
);

create table if not exists SectionSet
(
    stockNumber integer
        references Stock,
    sectionId   integer
        references Section,
    primary key (stockNumber, sectionId)
);

create table if not exists Supply
(
    number            integer
        primary key,
    provider          text    not null,
    date              text    not null,
    isDeclined        integer not null,
    isApproved        integer not null,
    statusDescription text    not null
);

create table if not exists ConsignmentInSupply
(
    supplyNumber       integer
        references Supply,
    consignmentArticle text
        references Consignment,
    primary key (supplyNumber, consignmentArticle)
);

create table if not exists SupplyRejectionRequest
(
    id                integer
        primary key autoincrement ,
    supplyId          text    not null
        references Supply,
    creatorId         integer not null,
    processorId       integer,
    isProcessed       integer not null,
    isArchived        integer not null
);

