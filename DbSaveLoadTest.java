import model.*;
import org.apache.commons.lang3.Range;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

public class DbSaveLoadTest {

    DataManager dataManager = new DataManager(
            new StockListContainer(new Stock(1, "qqq")),
            new SupplyListContainer(),
            new SupplyRejectionRequestsContainer());
    UserInterfaceEmulator ui = new UserInterfaceEmulator();
    SupplyPlanningSystemInterfaceEmulator spi = new SupplyPlanningSystemInterfaceEmulator();

    Delivery del = new Delivery(dataManager, ui, spi);

    @Test
    void db_test_save() throws IOException, SQLException {
        Section sec1 = null;
        try {
            sec1 = new Section(1, 30, dataManager.stocks().get(1), 25, 0.55);
            Section sec2 = new Section(2, 10, dataManager.stocks().get(1), 15, 0.30);
            dataManager.stocks().get(1).getSections().add(sec1);
            dataManager.stocks().get(1).getSections().add(sec2);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Supply sup : spi.getTodaySupplies()) {
            dataManager.supplies().addSupply(sup);
        }

        del.delivery(1, true, 1);
        del.delivery(1, true, 2);

        ui.setAnswer("areGoodsInSupplyOkDialog", "false");
        del.delivery(1, false, 3);

        dataManager.supplies().getSupplyByNumber(3).getConsignments().get(0).writeOffByExpDate(1);

        try {
            dataManager.saveStock(
                    "test_database2.sqlite",
                    1);
        } catch (Exception e){
            File f= new File("test_database2.sqlite");           //file to be delete
            if(f.delete())
                System.out.printf("deleted");
            throw e;
        }


    }
}
